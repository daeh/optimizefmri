function [ xBF ] = dae_get_FIRbf( xBF )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

designResolution = xBF.dt;
TR = xBF.TR;
lengthOfFIRs = xBF.length;


FIRbasisFunction = zeros(lengthOfFIRs/designResolution,lengthOfFIRs/TR);
row = 1:TR/designResolution:lengthOfFIRs/designResolution;
column = 1:lengthOfFIRs/TR;
for impulse = 1:lengthOfFIRs/TR
    FIRbasisFunction(row(impulse),column(impulse)) = 1;
end

xBF.bf = FIRbasisFunction;

end

