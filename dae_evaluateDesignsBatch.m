function [ results, sortedConEffs ] = dae_evaluateDesignsBatch( convolvedDesignMatrices, numPredictorsPerCond, selectionContrasts )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
if nargin == 3
    columnselector = logical(repelem(sum(logical(selectionContrasts),1),1,numPredictorsPerCond));
    effSelector = 3;
end
if nargin < 3 || min(size(selectionContrasts)) < 1 % no contrasts provided
    effSelector = 2;
end
% try
% % Initialize multicore
% parpool local;
% end

nDesigns = numel(convolvedDesignMatrices);
results = cell(nDesigns,1);

% Evaluate efficiency, VIF and R2 for whole design matrix
for iDesign = 1:nDesigns % can be parfor
    [results{iDesign}.designEfficiency, results{iDesign}.designVIF, results{iDesign}.designR2] = dae_evaluateDesignSingle(convolvedDesignMatrices{iDesign}, numPredictorsPerCond);
end

sortedConEffs = zeros(nDesigns,effSelector);

% Calcualte the efficiencies of the contrasts if they were provided
if effSelector == 3
    for iDesign = 1:nDesigns  % can be parfor
        results{iDesign}.ContrastEfficiency = dae_calcEff(convolvedDesignMatrices{iDesign},numPredictorsPerCond,selectionContrasts);
        results{iDesign}.VIF = results{iDesign}.designVIF(columnselector);
        results{iDesign}.R2 = results{iDesign}.designR2(columnselector);

        % use summary statistic for design matrix ranking
        results{iDesign}.SummaryStatistic = dae_summaryStatistic(results{iDesign}.ContrastEfficiency);
        
        sortedConEffs(iDesign,:) = [iDesign, results{iDesign}.designEfficiency, results{iDesign}.SummaryStatistic];
    end
else
    for iDesign = 1:nDesigns
        sortedConEffs(iDesign,:) = [iDesign, results{iDesign}.designEfficiency];
    end
end

% sort designs by overall efficiency or by contrasts if they were provided
sortedConEffs = sortrows(sortedConEffs,-1*effSelector);


end

