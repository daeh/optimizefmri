function [Cfg] = dae_resume(intermediateCfg, saveName)


addpath(genpath('/mindhive/gablab/u/daeda/optfmri/reboot/OptimizeFMRI')); % add library path
addpath('/mindhive/gablab/u/daeda/software/spm12');

load(intermediateCfg); % loads Cfg, rngcurr, schedules

%% Run optimization
% Returns summary statistcs in Cfg{iCfg}.effs where first column is corresponding design matrix, second column is overall effiency of design matrix, and (if selection contrasts are supplied) third column is the summary statistic. If contrasts are provided, designs are ranked by the summary statistic, otherwise by the overall design efficiency.
Cfg = dae_runOptimization(Cfg, 'sequential', 100, rngcurr, schedules);

save(saveName,'Cfg');

end
