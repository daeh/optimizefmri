function xi_byContrast = dae_calcEff(X,numPredictorsPerCond,contrastMatrix)
% Function based on Liu & Frank, NeuroImage, 2004. "Efficiency, power, and entropy in event-related fMRI with multiple trial types. Part I: theory."
% 

numPredictors = size(X,2);
numConds = numPredictors/numPredictorsPerCond;

if nargin == 3
    D = contrastMatrix;
end
if nargin < 3
    D = generate_total_contrasts(numConds);
end

numContrasts = size(D,1);
contrastCovariance = zeros(numContrasts,1);

for n = 1:numContrasts  % can be parfor
    L = kron(D(n,:), eye(numPredictorsPerCond)); % expand contrasts by number of predictors
    contrastCovariance(n) = trace(L/(X'*X)*L');   % this is equal to  trace( L*inv(X'*X)*L' )
%     contrastCovariance(n) = contrastCovariance(n) / (D(n,:)*D(n,:)'); % normalize efficiency
end

xi_byContrast = contrastCovariance.^-1; % efficiency for each contrast tested
% xi_total = numContrasts/sum(contrastCovariance(:)); % mean efficiency across all possible contrasts. For full set of contrasts, numContrasts = (numConds+numConds*(numConds-1)/2)

end

function D = generate_total_contrasts(numConds)
n = 1;
D = zeros(n,numConds); % Generates D, a 1xQ row vector of every trial type and pairwise difference
for di = 1:numConds
    for dj = 1:di
        D(n,1+(di-1):di) = -1;
        D(n,1+(dj-1):dj) = 1;
        n = n + 1;
    end
end

end
