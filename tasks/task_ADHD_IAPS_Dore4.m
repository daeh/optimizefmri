function [ InflatedSchedules, schedules ] = task_ADHD_IAPS_Dore4( nDesigns, scheduleTemporalResolution, schedules )
%UNTITLED2 Summary of this function goes here
%

InflatedSchedules = cell(nDesigns,1);

temporalInflationFactor = scheduleTemporalResolution^-1;

% 8 - 8 - 8, or 4-4 - 4-4 - 8
genBlock = [1;1;1;1;2;2;2;2;3;3;3;3;4;4;4;4;5;5;5;5;5;5;5;5];

blockLength = length(genBlock);

%{
1 = reapp - neg, disgust
2 = reapp - neg, threat
3 = look - neg, disgust
4 = look - neg, threat
5 = neutral
%}

ISIOptions = [2 4];
ITIOptions = [2 4];
genISI = repmat(ISIOptions,[1,blockLength/length(ISIOptions)])';
genITI = repmat(ITIOptions,[1,blockLength/length(ISIOptions)])';


if exist('schedules','var')
    nDesigns = size(schedules,2);
    iDesign = nDesigns;
    returnOnlyOne = true;
else
    [temp_schedule, stimOrder, ISIOrder, ITIOrder] = randParam(genBlock, genISI, genITI);
    schedules = single(NaN(size(temp_schedule,1),nDesigns));
    iDesign = 1;
    returnOnlyOne = false;
end

while iDesign <= nDesigns
    clearvars stimOrderBlockRand ISIBlockRand ITIBlockRand ISI ITI ISIOrder
    
    % handel sequential vs bulk variable return
    if ~returnOnlyOne
        iSchedule = iDesign;
    else
        iSchedule = 1;
    end
    
    runImmediately = true; % generate first schedule before checking for uniqueness
    while runImmediately || any( all( bsxfun( @eq, schedules, single(temp_schedule) ), 1) )
        runImmediately = false;
        [temp_schedule, stimOrder, ISIOrder, ITIOrder] = randParam(genBlock, genISI, genITI);
    end
        
	schedules(:,iDesign) = temp_schedule;
    
    
    SPM = single(zeros(1,7));
    iTime = 1;
    iStim = 1;
    iISI = 1;
    iITI = 1;
    
    duration = 8*temporalInflationFactor - 1;
    SPM(iTime:iTime+duration,6) = 0; % prescan interval
    iTime = iTime+duration + 1;
    
    for iTrial = 1:length(stimOrder)
        duration = ITIOrder(iITI)*temporalInflationFactor - 1;
        % SPM(iTime:iTime+duration,9) = 1; % ITI
        iTime = iTime+duration + 1;
        iITI = iITI + 1;
        
        duration = 2*temporalInflationFactor - 1;
        SPM(iTime:iTime+duration,6) = 1; % instructions
        iTime = iTime+duration + 1;
        
        %{
        1 = reapp - neg, disgust
        2 = reapp - neg, threat
        3 = look - neg, disgust
        4 = look - neg, threat
        5 = neutral
        %}
        duration = 8*temporalInflationFactor - 1;
        if stimOrder(iStim) == 1
            SPM(iTime:iTime+duration,[1]) = 1; % IAPS
        elseif stimOrder(iStim) == 2
            SPM(iTime:iTime+duration,[2]) = 1; % IAPS
        elseif stimOrder(iStim) == 3
            SPM(iTime:iTime+duration,[3]) = 1; % IAPS
        elseif stimOrder(iStim) == 4
            SPM(iTime:iTime+duration,[4]) = 1; % IAPS
        elseif stimOrder(iStim) == 5
            SPM(iTime:iTime+duration,[5]) = 1; % IAPS
        else
            error('stim not rec')
        end
        iTime = iTime+duration + 1;
        iStim = iStim + 1;
        
        duration = ISIOrder(iISI)*temporalInflationFactor - 1;
        % SPM(iTime:iTime+duration,8) = 1; % ISI
        iTime = iTime+duration + 1;
        iISI = iISI + 1;
        
        duration = 4*temporalInflationFactor - 1;
        SPM(iTime:iTime+duration,7) = 1; % Rating
        iTime = iTime+duration + 1;
    end
    
    InflatedSchedules{iSchedule} = SPM;
    totalTime = (iTime - 1)*scheduleTemporalResolution;
    
    % print # of generated schedules
    if ~mod(iDesign,1000)
        fprintf('%0.0f  ',iDesign);
    end
    
    iDesign = iDesign + 1;
    
end



end

function [temp_schedule, stimOrder, ISIOrder, ITIOrder] = randParam(genBlock, genISI, genITI)
    stimOrder = genBlock(randperm(length(genBlock))); % randomize within block
    ISIOrder = genISI(randperm(length(genISI))); % randomize within block

    ITIOrder = zeros(size(genITI));
    % since all runs start with a 4s ITI
    while ITIOrder(1) ~= 2
        ITIOrder = genITI(randperm(length(genITI))); % randomize within block
    end
    
    temp_schedule = [stimOrder;ISIOrder;ITIOrder];
end