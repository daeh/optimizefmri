function [ Cfg ] = dae_runOptimization( Cfg, behavior, nTopDesigns, rngseed, schedules_temp )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if nargin >= 5
    existingDesigns = size(schedules_temp,2);
elseif nargin < 5
    existingDesigns = 0;
end
if nargin < 4
    rngseed = 'default';
end
if nargin < 3
    nTopDesigns = 0;
end

rng(rngseed)

% normalize contrasts
for iCfg = 1:numel(Cfg)
    if ~isfield(Cfg{iCfg},'selectionContrastsWeights')
        Cfg{iCfg}.selectionContrastsWeights = ones(size(Cfg{iCfg}.selectionContrasts,1),1);
    end
    if size(Cfg{iCfg}.selectionContrastsWeights,1) ~= size(Cfg{iCfg}.selectionContrasts,1)
        error('weights matrix does not match contrasts matrix')
    end
    for iRow = 1:size(Cfg{iCfg}.selectionContrasts,1)
        Cfg{iCfg}.selectionContrasts(iRow,:) = Cfg{iCfg}.selectionContrastsWeights(iRow,1) * Cfg{iCfg}.selectionContrasts(iRow,:)./sqrt( Cfg{iCfg}.selectionContrasts(iRow,:) * Cfg{iCfg}.selectionContrasts(iRow,:)' );
    end
end

%% Initialize multicore
% parpool local

if strcmpi(behavior,'bulk')
    
    for iCfg = 1:numel(Cfg)
        if isnumeric(Cfg{iCfg}.genSchedule) % assign existing schedules
            Cfg{iCfg}.InflatedSchedules = Cfg{ Cfg{iCfg}.genSchedule }.InflatedSchedules;
            Cfg{iCfg}.lastConditionNull = Cfg{ Cfg{iCfg}.genSchedule }.lastConditionNull;
        else % generate novel stim schedules
            [Cfg{iCfg}.InflatedSchedules, ~] = feval(Cfg{iCfg}.genSchedule, Cfg{iCfg}.nDesigns, Cfg{iCfg}.xBF.dt ); % for bulk behavior, task function insure that all schedules are novel (no resampling)
            Cfg{iCfg}.lastConditionNull = 0; % select 1 if the last conditions is null events, and thus should not be convolved
        end
        
        % Convolve designs
        Cfg{iCfg}.convolvedInflatedDesignMatrices = dae_convolveDesigns( Cfg{iCfg}.InflatedSchedules, Cfg{iCfg}.xBF.bf, Cfg{iCfg}.lastConditionNull );
        
        % Downsample convolved designs
        Cfg{iCfg}.DesignMatrices = dae_downsampleMatrices(Cfg{iCfg}.convolvedInflatedDesignMatrices, Cfg{iCfg}.TR, Cfg{iCfg}.xBF.dt, Cfg{iCfg}.downsampleBehavior);
        Cfg{iCfg} = rmfield(Cfg{iCfg},'convolvedInflatedDesignMatrices');
        
        % Evaluate the efficiency of the designs
        [ Cfg{iCfg}.designEfficiency, Cfg{iCfg}.effs ] = dae_evaluateDesignsBatch( Cfg{iCfg}.DesignMatrices, Cfg{iCfg}.numPredictorsPerCond, Cfg{iCfg}.selectionContrasts );
        Cfg{iCfg} = rmfield(Cfg{iCfg},'DesignMatrices');
        
    end
    
elseif strcmpi(behavior,'sequential')
    
    for iCfg = 1:numel(Cfg)
        
        if isfield(Cfg{iCfg},'selectionContrasts')
            effSelector = 3;
        else
            effSelector = 2;
        end
        
        if nTopDesigns == 0
            nTopDesigns = Cfg{iCfg}.nDesigns;
        end
        
        Top.Effs = [(1:nTopDesigns)',zeros(nTopDesigns,effSelector-1)];
        Top.DesignEfficiency = cell(nTopDesigns,1);
        Top.InflatedSchedules = cell(nTopDesigns,1);
        
        Cfg{iCfg}.lastConditionNull = 0; % select 1 if the last conditions is null events, and thus should not be convolved
        
        fprintf('\nDesigns evaluated: \n');
        % Generate first design
        [temp.InflatedSchedules, temp_schedule] = feval( Cfg{iCfg}.genSchedule, 1, Cfg{iCfg}.xBF.dt );
        
        schedules = single(NaN(size(temp_schedule,1),Cfg{iCfg}.nDesigns));
        if exist('schedules_temp','var')
            schedules(:,1:existingDesigns) = schedules_temp;
            clearvars schedules_temp;
        end
        
        clearvars temp_schedule temp
        for iDesign = existingDesigns+1:Cfg{iCfg}.nDesigns % number of unique schedules to evaluate
            
            % test if schedule has been used before (whereas task function insures no resampling in bulk behavior, this ensures no resampling for sequential behavior)
            [temp.InflatedSchedules, schedules(:,1:iDesign)] = feval( Cfg{iCfg}.genSchedule, 1, Cfg{iCfg}.xBF.dt, schedules(:,1:iDesign) );
            
            % Convolve designs
            temp.convolvedInflatedDesignMatrices = dae_convolveDesigns( temp.InflatedSchedules, Cfg{iCfg}.xBF.bf, Cfg{iCfg}.lastConditionNull );
            
            % Downsample convolved designs
            temp.DesignMatrices = dae_downsampleMatrices(temp.convolvedInflatedDesignMatrices, Cfg{iCfg}.TR, Cfg{iCfg}.xBF.dt, Cfg{iCfg}.downsampleBehavior);
            
            % Evaluate the efficiency of the designs
            [ temp.designEfficiency, temp.effs ] = dae_evaluateDesignsBatch( temp.DesignMatrices, Cfg{iCfg}.numPredictorsPerCond, Cfg{iCfg}.selectionContrasts );
            
            % replace lowest scoring design with current design if current design is better
            if temp.effs(effSelector) > Top.Effs(1,effSelector)
                Top.DesignEfficiency(Top.Effs(1,1),1) = temp.designEfficiency;
                Top.InflatedSchedules(Top.Effs(1,1),1) = temp.InflatedSchedules;
                Top.Effs(1,:) = [Top.Effs(1,1), temp.effs(2:end)];
                Top.Effs = sortrows(Top.Effs,effSelector);
            end
            clearvars temp
            
            % print progress
            if (mod(iDesign,1000)==0 && iDesign < 10000) || (mod(iDesign,10000)==0 && iDesign)
                fprintf('%d  \n',iDesign);
                try
                    intermediateCfgSave(Top,nTopDesigns,Cfg,iCfg,schedules(:,1:iDesign),rng);
                end
            end
            
            
        end
        
        Cfg = intermediateCfgSave(Top,nTopDesigns,Cfg,iCfg,schedules,rng);
        clearvars Top
        
    end
    
elseif strcmpi(behavior,'slurm')
    
    
    
    
    for iCfg = 1:numel(Cfg)
        if isnumeric(Cfg{iCfg}.genSchedule) % assign existing schedules
            Cfg{iCfg}.InflatedSchedules = Cfg{ Cfg{iCfg}.genSchedule }.InflatedSchedules;
            Cfg{iCfg}.lastConditionNull = Cfg{ Cfg{iCfg}.genSchedule }.lastConditionNull;
        else % generate novel stim schedules
            [Cfg{iCfg}.InflatedSchedules, ~] = feval(Cfg{iCfg}.genSchedule, Cfg{iCfg}.nDesigns, Cfg{iCfg}.xBF.dt ); % for bulk behavior, task function insure that all schedules are novel (no resampling)
            Cfg{iCfg}.lastConditionNull = 0; % select 1 if the last conditions is null events, and thus should not be convolved
        end
        
        section_size = runBehavior.tasks_per_node * runBehavior.nodes;
        
        partitions = floor(Cfg{iCfg}.nDesigns / section_size) : floor(Cfg{iCfg}.nDesigns / section_size) : Cfg{iCfg}.nDesigns;
        partitions(end) = Cfg{iCfg}.nDesigns;

        save(sprintf('Cfg%03d.mat',iCfg),'Cfg{iCfg}.InflatedSchedules')
        
%         for iPart = 1:length(partitions)
%             save(sprintf('Cfg%03d_%03d.mat',iCfg,iPart),'Cfg{iCfg}.InflatedSchedules')
%         end
        
%         dir_Cfg = sprintf('slurm_Cfg%03d',iCfg);
%         
%         mkdir(dir_Cfg);
%         
%         cd(dir_Cfg);
%         
%         for iPart = 1:length(partitions)
%             dir_part = sprintf('part_03d',iPart);
%             mkdir(dir_part);
%             cd(dir_part);
%             
%         end
        
%         % Convolve designs
%         Cfg{iCfg}.convolvedInflatedDesignMatrices = dae_convolveDesigns( Cfg{iCfg}.InflatedSchedules, Cfg{iCfg}.xBF.bf, Cfg{iCfg}.lastConditionNull );
%         
%         % Downsample convolved designs
%         Cfg{iCfg}.DesignMatrices = dae_downsampleMatrices(Cfg{iCfg}.convolvedInflatedDesignMatrices, Cfg{iCfg}.TR, Cfg{iCfg}.xBF.dt, Cfg{iCfg}.downsampleBehavior);
%         Cfg{iCfg} = rmfield(Cfg{iCfg},'convolvedInflatedDesignMatrices');
%         
%         % Evaluate the efficiency of the designs
%         [ Cfg{iCfg}.designEfficiency, Cfg{iCfg}.effs ] = dae_evaluateDesignsBatch( Cfg{iCfg}.DesignMatrices, Cfg{iCfg}.numPredictorsPerCond, Cfg{iCfg}.selectionContrasts );
%         Cfg{iCfg} = rmfield(Cfg{iCfg},'DesignMatrices');
%         
    end
    
    
    
else
    error('run behavior not understood');
end


end


function Cfg = intermediateCfgSave(Top,nTopDesigns,Cfg,iCfg,schedules,rngcurr)
    Top.Effs = flipud(Top.Effs);
    Top.idx = Top.Effs(:,1);
    Cfg{iCfg}.effs = [(1:nTopDesigns)', Top.Effs(:,2:end)];
    Cfg{iCfg}.designEfficiency = Top.DesignEfficiency(Top.idx);
    Cfg{iCfg}.InflatedSchedules = Top.InflatedSchedules(Top.idx);
    save('intermediateCfg','Cfg','schedules','rngcurr','-v7.3')
end