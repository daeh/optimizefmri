function [Cfg] = wrapper_ADHD_Dore4(saveName, nDesigns)

addpath(genpath('/mindhive/gablab/u/daeda/optfmri/OptimizeFMRI')); % add library path
addpath('/mindhive/gablab/u/daeda/software/spm12');

%% Define Cfg
% if using 'sequential' runOptimization paramater, must supply design generating function for each Cfg. To test multiple variations (e.g. different basis functions) on the same designs, use 'bulk' behavior and specify which Cfg # to take designs from at Cfg{iCfg}.genSchedule
iCfg = 0;

% Common parameters
TR = 2; % EPI volume collection time in seconds
dt = 0.1; % Inflated design temporal resolution in seconds

selectionContrasts = ...
    [1 1 -1 -1 0 0 0; ... % reapp vs look
    1 -1 1 -1 0 0 0; ... % self vs other
    1 0 -1 0 0 0 0; ... % reapp self vs reapp other
    0 1 0 -1 0 0 0]; % look self vs look other
% 1 1 0 0 -2 0 0; ... % reapp vs neu
% 0 0 1 1 -2 0 0; ... % look vs neu

selectionContrastsWeights = [1;1;1;1]; % Set relative weights for contrasts. Set to ones or leave blank to set to equal weight.

% HRF with time and dispersion derivatives
iCfg = iCfg+1;
Cfg{iCfg}.name = 'Dore4 HRF';
Cfg{iCfg}.TR = TR; % EPI volume collection time in seconds
% Specify design parameters
Cfg{iCfg}.genSchedule = 'task_ADHD_IAPS_Dore4'; % supply design generating fuction or which Cfg # to take generated designs from
Cfg{iCfg}.nDesigns = nDesigns;
% supply basis function
Cfg{iCfg}.convFunction = 'spm_get_bf'; % This returns the canonical and two derivatives in the matrix "xBF.bf" (type "help spm_get_bf" for more info). hrf column is the same as spm_hrf(designResolution).
    Cfg{iCfg}.xBF.name = 'hrf'; % hardcoded description of basis functions specified
    % Cfg{iCfg}.xBF.name = 'hrf (with time derivative)'; % hardcoded description of basis functions specified
    % Cfg{iCfg}.xBF.name = 'hrf (with time and dispersion derivatives)'; % hardcoded description of basis functions specified
Cfg{iCfg}.xBF.dt = dt; % Inflated design temporal resolution in seconds
Cfg{iCfg}.xBF.length = 32; % window length (seconds)
Cfg{iCfg}.xBF.order = 1;
% Specify downsampling behavior
Cfg{iCfg}.downsampleBehavior = 'sample';
% Specify contrasts used to generate a summary statistic used to rank design efficiencies
Cfg{iCfg}.selectionContrasts = selectionContrasts; % Specify contrasts or set to [] if every independant and pairwise contrast should be used to rank designs
Cfg{iCfg}.selectionContrastsWeights = selectionContrastsWeights;

%{
% Common parameters
TR = 2; % EPI volume collection time in seconds
dt = 0.1; % Inflated design temporal resolution in seconds
selectionContrasts = ...
    [1 1 -1 -1 0 0 0 0 0; ... % reapp vs look
    1 -1 1 -1 0 0 0 0 0; ... % disgust vs threat
    1 0 -1 0 0 0 0 0 0; ... % reapp disgust vs reapp threat
    0 1 0 -1 0 0 0 0 0]; % look disgust vs look threat
% 1 1 0 0 -2 0 0 0 0; ... % reapp vs neu
% 0 0 1 1 -2 0 0 0 0; ... % look vs neu


% HRF with time and dispersion derivatives
iCfg = 1;
Cfg{iCfg}.name = 'Dore1 HRF';
Cfg{iCfg}.TR = TR; % EPI volume collection time in seconds
% Specify design parameters
Cfg{iCfg}.genSchedule = 'task_ADHD_IAPS_Dore1'; % supply design generating fuction or which Cfg # to take generated designs from
Cfg{iCfg}.nDesigns = 100000;
% supply basis function
Cfg{iCfg}.convFunction = 'spm_get_bf'; % This returns the canonical and two derivatives in the matrix "xBF.bf" (type "help spm_get_bf" for more info). hrf column is the same as spm_hrf(designResolution).
    Cfg{iCfg}.xBF.name = 'hrf'; % hardcoded description of basis functions specified
%     Cfg{iCfg}.xBF.name = 'hrf (with time derivative)'; % hardcoded description of basis functions specified
%     Cfg{iCfg}.xBF.name = 'hrf (with time and dispersion derivatives)'; % hardcoded description of basis functions specified
Cfg{iCfg}.xBF.dt = dt; % Inflated design temporal resolution in seconds
Cfg{iCfg}.xBF.length = 32; % window length (seconds)
Cfg{iCfg}.xBF.order = 1;
% Specify downsampling behavior
Cfg{iCfg}.downsampleBehavior = 'sample';
% Specify contrasts used to generate a summary statistic used to rank design efficiencies
Cfg{iCfg}.selectionContrasts = selectionContrasts; % Specify contrasts or set to [] if every independant and pairwise contrast should be used to rank designs
Cfg{iCfg}.selectionContrastsWeights = selectionContrastsWeights;

% HRF with time and dispersion derivatives
iCfg = iCfg + 1;
Cfg{iCfg}.name = 'Dore1 HRF';
Cfg{iCfg}.TR = TR; % EPI volume collection time in seconds
% Specify design parameters
Cfg{iCfg}.genSchedule = 1; % supply design generating fuction or which Cfg # to take generated designs from
Cfg{iCfg}.nDesigns = 1000;
% supply basis function
Cfg{iCfg}.convFunction = 'spm_get_bf'; % This returns the canonical and two derivatives in the matrix "xBF.bf" (type "help spm_get_bf" for more info). hrf column is the same as spm_hrf(designResolution).
%     Cfg{iCfg}.xBF.name = 'hrf'; % hardcoded description of basis functions specified
    Cfg{iCfg}.xBF.name = 'hrf (with time derivative)'; % hardcoded description of basis functions specified
%     Cfg{iCfg}.xBF.name = 'hrf (with time and dispersion derivatives)'; % hardcoded description of basis functions specified
Cfg{iCfg}.xBF.dt = dt; % Inflated design temporal resolution in seconds
Cfg{iCfg}.xBF.length = 32; % window length (seconds)
Cfg{iCfg}.xBF.order = 1;
% Specify downsampling behavior
Cfg{iCfg}.downsampleBehavior = 'sample';
% Specify contrasts used to generate a summary statistic used to rank design efficiencies
Cfg{iCfg}.selectionContrasts = selectionContrasts; % Specify contrasts or set to [] if every independant and pairwise contrast should be used to rank designs
Cfg{iCfg}.selectionContrastsWeights = selectionContrastsWeights;

% HRF with time and dispersion derivatives
iCfg = iCfg + 1;
Cfg{iCfg}.name = 'Dore1 HRF';
Cfg{iCfg}.TR = TR; % EPI volume collection time in seconds
% Specify design parameters
Cfg{iCfg}.genSchedule = 1; % supply design generating fuction or which Cfg # to take generated designs from
Cfg{iCfg}.nDesigns = 1000;
% supply basis function
Cfg{iCfg}.convFunction = 'spm_get_bf'; % This returns the canonical and two derivatives in the matrix "xBF.bf" (type "help spm_get_bf" for more info). hrf column is the same as spm_hrf(designResolution).
%     Cfg{iCfg}.xBF.name = 'hrf'; % hardcoded description of basis functions specified
%     Cfg{iCfg}.xBF.name = 'hrf (with time derivative)'; % hardcoded description of basis functions specified
    Cfg{iCfg}.xBF.name = 'hrf (with time and dispersion derivatives)'; % hardcoded description of basis functions specified
Cfg{iCfg}.xBF.dt = dt; % Inflated design temporal resolution in seconds
Cfg{iCfg}.xBF.length = 32; % window length (seconds)
Cfg{iCfg}.xBF.order = 1;
% Specify downsampling behavior
Cfg{iCfg}.downsampleBehavior = 'sample';
% Specify contrasts used to generate a summary statistic used to rank design efficiencies
Cfg{iCfg}.selectionContrasts = selectionContrasts; % Specify contrasts or set to [] if every independant and pairwise contrast should be used to rank designs
Cfg{iCfg}.selectionContrastsWeights = selectionContrastsWeights;

selectionContrasts = ...
    [1 1 -1 -1 0 0 0; ... % reapp vs look
    1 -1 1 -1 0 0 0; ... % disgust vs threat
    1 0 -1 0 0 0 0; ... % reapp disgust vs reapp threat
    0 1 0 -1 0 0 0]; % look disgust vs look threat

% HRF with time and dispersion derivatives
iCfg = 4;
Cfg{iCfg}.name = 'Dore2 HRF';
Cfg{iCfg}.TR = TR; % EPI volume collection time in seconds
% Specify design parameters
Cfg{iCfg}.genSchedule = 'task_ADHD_IAPS_Dore2'; % supply design generating fuction or which Cfg # to take generated designs from
Cfg{iCfg}.nDesigns = 1000;
% supply basis function
Cfg{iCfg}.convFunction = 'spm_get_bf'; % This returns the canonical and two derivatives in the matrix "xBF.bf" (type "help spm_get_bf" for more info). hrf column is the same as spm_hrf(designResolution).
    Cfg{iCfg}.xBF.name = 'hrf'; % hardcoded description of basis functions specified
%     Cfg{iCfg}.xBF.name = 'hrf (with time derivative)'; % hardcoded description of basis functions specified
%     Cfg{iCfg}.xBF.name = 'hrf (with time and dispersion derivatives)'; % hardcoded description of basis functions specified
Cfg{iCfg}.xBF.dt = dt; % Inflated design temporal resolution in seconds
Cfg{iCfg}.xBF.length = 32; % window length (seconds)
Cfg{iCfg}.xBF.order = 1;
% Specify downsampling behavior
Cfg{iCfg}.downsampleBehavior = 'sample';
% Specify contrasts used to generate a summary statistic used to rank design efficiencies
Cfg{iCfg}.selectionContrasts = selectionContrasts; % Specify contrasts or set to [] if every independant and pairwise contrast should be used to rank designs
Cfg{iCfg}.selectionContrastsWeights = selectionContrastsWeights;

% HRF with time and dispersion derivatives
iCfg = iCfg + 1;
Cfg{iCfg}.name = 'Dore2 HRF';
Cfg{iCfg}.TR = TR; % EPI volume collection time in seconds
% Specify design parameters
Cfg{iCfg}.genSchedule = 4; % supply design generating fuction or which Cfg # to take generated designs from
Cfg{iCfg}.nDesigns = 1000;
% supply basis function
Cfg{iCfg}.convFunction = 'spm_get_bf'; % This returns the canonical and two derivatives in the matrix "xBF.bf" (type "help spm_get_bf" for more info). hrf column is the same as spm_hrf(designResolution).
%     Cfg{iCfg}.xBF.name = 'hrf'; % hardcoded description of basis functions specified
    Cfg{iCfg}.xBF.name = 'hrf (with time derivative)'; % hardcoded description of basis functions specified
%     Cfg{iCfg}.xBF.name = 'hrf (with time and dispersion derivatives)'; % hardcoded description of basis functions specified
Cfg{iCfg}.xBF.dt = dt; % Inflated design temporal resolution in seconds
Cfg{iCfg}.xBF.length = 32; % window length (seconds)
Cfg{iCfg}.xBF.order = 1;
% Specify downsampling behavior
Cfg{iCfg}.downsampleBehavior = 'sample';
% Specify contrasts used to generate a summary statistic used to rank design efficiencies
Cfg{iCfg}.selectionContrasts = selectionContrasts; % Specify contrasts or set to [] if every independant and pairwise contrast should be used to rank designs
Cfg{iCfg}.selectionContrastsWeights = selectionContrastsWeights;

% HRF with time and dispersion derivatives
iCfg = iCfg + 1;
Cfg{iCfg}.name = 'Dore2 HRF';
Cfg{iCfg}.TR = TR; % EPI volume collection time in seconds
% Specify design parameters
Cfg{iCfg}.genSchedule = 4; % supply design generating fuction or which Cfg # to take generated designs from
Cfg{iCfg}.nDesigns = 1000;
% supply basis function
Cfg{iCfg}.convFunction = 'spm_get_bf'; % This returns the canonical and two derivatives in the matrix "xBF.bf" (type "help spm_get_bf" for more info). hrf column is the same as spm_hrf(designResolution).
%     Cfg{iCfg}.xBF.name = 'hrf'; % hardcoded description of basis functions specified
%     Cfg{iCfg}.xBF.name = 'hrf (with time derivative)'; % hardcoded description of basis functions specified
    Cfg{iCfg}.xBF.name = 'hrf (with time and dispersion derivatives)'; % hardcoded description of basis functions specified
Cfg{iCfg}.xBF.dt = dt; % Inflated design temporal resolution in seconds
Cfg{iCfg}.xBF.length = 32; % window length (seconds)
Cfg{iCfg}.xBF.order = 1;
% Specify downsampling behavior
Cfg{iCfg}.downsampleBehavior = 'sample';
% Specify contrasts used to generate a summary statistic used to rank design efficiencies
Cfg{iCfg}.selectionContrasts = selectionContrasts; % Specify contrasts or set to [] if every independant and pairwise contrast should be used to rank designs
Cfg{iCfg}.selectionContrastsWeights = selectionContrastsWeights;

% FIR
iCfg = iCfg + 1;
Cfg{iCfg}.name = 'Dore FIR';
Cfg{iCfg}.TR = TR; % EPI volume collection time in seconds
% Specify design parameters
Cfg{iCfg}.genSchedule = 4; % supply design generating fuction or which Cfg # to take generated designs from
Cfg{iCfg}.nDesigns = 1000;
% supply basis generating fuction or which Cfg # to take basis from
Cfg{iCfg}.convFunction = 'dae_get_FIRbf'; % supply basis generating fuction or which Cfg # to take basis from
Cfg{iCfg}.xBF.dt = dt; % Inflated design temporal resolution in seconds
Cfg{iCfg}.xBF.length = 10; % window length (seconds)
Cfg{iCfg}.xBF.TR = TR;
% Specify downsampling behavior
Cfg{iCfg}.downsampleBehavior = 'mean';
% Specify contrasts used to generate a summary statistic used to rank design efficiencies
Cfg{iCfg}.selectionContrasts = selectionContrasts; % Specify contrasts or set to [] if every independant and pairwise contrast should be used to rank designs
Cfg{iCfg}.selectionContrastsWeights = selectionContrastsWeights;

%{
% HRF with time derivative
iCfg = iCfg + 1; %2
Cfg{iCfg}.name = 'Uchida HRF';
Cfg{iCfg}.TR = TR;
% Specify design parameters
Cfg{iCfg}.genSchedule = 'task_ADHD_IAPS_Uchida2_multicondition_singlecolumn'; % supply design generating fuction or which Cfg # to take generated designs from
Cfg{iCfg}.nDesigns = 100;
% supply basis fuction
Cfg{iCfg}.convFunction = 'spm_get_bf';  % supply basis generating fuction or which Cfg # to take basis from
Cfg{iCfg}.xBF.name = 'hrf'; % hardcoded description of basis functions specified
Cfg{iCfg}.xBF.dt = dt; % Inflated design temporal resolution in seconds
Cfg{iCfg}.xBF.length = 32; % window length (seconds)
Cfg{iCfg}.xBF.order = 1;
% Specify downsampling behavior
Cfg{iCfg}.downsampleBehavior = 'sample';
% Specify contrasts used to generate a summary statistic used to rank design efficiencies
Cfg{iCfg}.selectionContrasts = selectionContrasts; % Specify contrasts or set to [] if every independant and pairwise contrast should be used to rank designs
Cfg{iCfg}.selectionContrastsWeights = selectionContrastsWeights;
%}
%{
% FIR
iCfg = 1;
Cfg{iCfg}.name = 'Dore FIR';
Cfg{iCfg}.TR = TR; % EPI volume collection time in seconds
% Specify design parameters
Cfg{iCfg}.genSchedule = 'task_ADHD_IAPS_Dore1'; % supply design generating fuction or which Cfg # to take generated designs from
Cfg{iCfg}.nDesigns = 1000;
% supply basis generating fuction or which Cfg # to take basis from
Cfg{iCfg}.convFunction = 'dae_get_FIRbf'; % supply basis generating fuction or which Cfg # to take basis from
Cfg{iCfg}.xBF.dt = dt; % Inflated design temporal resolution in seconds
Cfg{iCfg}.xBF.length = 10; % window length (seconds)
Cfg{iCfg}.xBF.TR = TR;
% Specify downsampling behavior
Cfg{iCfg}.downsampleBehavior = 'mean';
% Specify contrasts used to generate a summary statistic used to rank design efficiencies
Cfg{iCfg}.selectionContrasts = selectionContrasts; % Specify contrasts or set to [] if every independant and pairwise contrast should be used to rank designs
Cfg{iCfg}.selectionContrastsWeights = selectionContrastsWeights;
%}


%}

clearvars -except Cfg saveName

%% Define basis functions for convolution
for iCfg = 1:numel(Cfg)
    if isnumeric(Cfg{iCfg}.convFunction) % assign existing basis functions
        Cfg{iCfg}.xBF = Cfg{ Cfg{iCfg}.convFunction }.xBF;
        Cfg{iCfg}.numPredictorsPerCond = Cfg{ Cfg{iCfg}.convFunction }.numPredictorsPerCond;
    else % generate basis functions
        Cfg{iCfg}.xBF = feval( Cfg{iCfg}.convFunction, Cfg{iCfg}.xBF );
        Cfg{iCfg}.numPredictorsPerCond = size(Cfg{iCfg}.xBF.bf,2);
    end
end

% Generate designs in bulk and run (good for smaller numbers of designs)
% runBehavior = 'bulk';
% nTopDesigns = 0;

% Generate designs sequentially and save nTopDesigns (good for large numbers of designs)
runBehavior = 'sequential';
nTopDesigns = 100; % set to zero to return evaluations of all generated designs

%% Run optimization
% Returns summary statistcs in Cfg{iCfg}.effs where first column is corresponding design matrix, second column is overall effiency of design matrix, and (if selection contrasts are supplied) third column is the summary statistic. If contrasts are provided, designs are ranked by the summary statistic, otherwise by the overall design efficiency.
Cfg = dae_runOptimization(Cfg, runBehavior, nTopDesigns);


save(saveName,'Cfg');

end
