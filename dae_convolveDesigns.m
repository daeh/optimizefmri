function convolvedDesignMatrices = dae_convolveDesigns(designMatrices, basisVectors, lastConditionNull)
% fuction convolves every column of a schedule maxtrix with every column vector of a basis function matrix. Thus, if there are D conditions and BF basis functions, the resulting matrix with have D*BF predictor columns.

nDesigns = numel(designMatrices);
numPredictorsPerCond = size(basisVectors,2);
convolvedDesignMatrices = cell(nDesigns,1);

for iDesign = 1:nDesigns % can be parfor
    if lastConditionNull
        convolvedDesignMatrices{iDesign} = zeros(size(designMatrices{iDesign},1)+size(basisVectors,1)-1, (size(designMatrices{iDesign},2)-1)*numPredictorsPerCond); % initalize matrix
        for iCondition = 1:size(designMatrices{iDesign},2)-1
            for iPredictor = 1:numPredictorsPerCond
                convolvedDesignMatrices{iDesign}( :, (iCondition-1) * numPredictorsPerCond + iPredictor ) = conv( designMatrices{iDesign}(:,iCondition), basisVectors(:,iPredictor) ); 
            end
        end
    else
        convolvedDesignMatrices{iDesign} = zeros(size(designMatrices{iDesign},1)+size(basisVectors,1)-1, size(designMatrices{iDesign},2)*numPredictorsPerCond); % initalize matrix
        for iCondition = 1:size(designMatrices{iDesign},2)
            for iPredictor = 1:numPredictorsPerCond
                convolvedDesignMatrices{iDesign}( :, (iCondition-1) * numPredictorsPerCond + iPredictor ) = conv( designMatrices{iDesign}(:,iCondition), basisVectors(:,iPredictor) ); 
            end
        end        
    end
end