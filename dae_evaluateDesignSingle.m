function [designEfficiency designVIF designR2] = dae_evaluateDesignSingle(convolvedDesignMatrix,numPredictorsPerCond)

    designEfficiency = mean(dae_calcEff(convolvedDesignMatrix,numPredictorsPerCond));
    [designVIF, designR2] = myBatch3_calcVIF(convolvedDesignMatrix); % {iMatrix}(:,1:end-1));

end