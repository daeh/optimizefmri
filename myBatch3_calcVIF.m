function [VIF R2] = myBatch3_calcVIF(X)

% variance inflation factor related to correlation between regressors
% original function by Talia Konkle, modified by Stefano Anzellotti on
% 2013-08-29

%% Initialize

sse = @(x)(sum((x-mean(x)).^2));
VIF = []; 
R2 = [];
nConditions = size(X,2);

%% Calculate the VIF for each condition

for iCondition=1:nConditions
%     disp(['getting vif for cond: ' num2str(i)])
    predictor_i = X(:,iCondition);   % this predictor
    predictor_rem =  X;     % remaining predictors
    predictor_rem(:,iCondition) = [];
    sstot = sse(predictor_i);
    [coefficients,coefficients_interval,residuals] = regress(predictor_i, predictor_rem);
    ssresid = sse(residuals);
%     r2 = 1-ssresid/sstot;
    r2 = 1-min(ssresid,sstot)/sstot; % need the min because regress is not precise
    % save it:
    R2(iCondition) = r2;
    VIF(iCondition) = 1/(1-r2);
end

end



