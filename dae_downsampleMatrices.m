function downsampledMatrices = dae_downsampleMatrices(InflatedMatrices, TR, designResolution, behavior)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% if nargin < 4
%     terminationpoint = 0;
% end
if nargin < 3
    behavior = 'sample'; % samples last microtime to construct TR;
end

offsetTR = 0; % Where to sample within the TR. Set to 0 to sample last microtimepoint, set to (inflatedTR + 1) to sample first

inflatedTR = TR/designResolution;
downsampledMatrices = cell(size(InflatedMatrices));
% finalTR = size(InflatedMatrices{iDesign},1) - terminationpoint/designResolution; % How many TRs to follow onset of final trial

for iDesign = 1:numel(InflatedMatrices) % can be parfor
    sampledTimePoints = inflatedTR - offsetTR : inflatedTR : size(InflatedMatrices{iDesign},1);
    if strcmpi(behavior,'sample')
        downsampledMatrices{iDesign} = InflatedMatrices{iDesign}(sampledTimePoints,:);
    elseif strcmpi(behavior,'mean') % take weighted mean of microtime predictors to construct TR
        downsampledMatrices{iDesign} = zeros( length(sampledTimePoints), size(InflatedMatrices{iDesign}, 2) );
        sampledTimePoints = [0, sampledTimePoints];
        for iBin = 2:length(sampledTimePoints)
            downsampledMatrices{iDesign}( iBin-1, : ) = mean( InflatedMatrices{iDesign}( sampledTimePoints(iBin-1) + 1 : sampledTimePoints(iBin), : ), 1);
        end
    end
end

end

