MaiNeu = [...
    2026, 1; ...
    2032, 1; ...
    2190, 1; ...
    2200, 1; ...
    2215, 1; ...
    2273, 1; ...
    2377, 1; ...
    2393, 1; ...
    2446, 1; ... % boots
    2480, 1; ...
    2512, 1; ...
    2570, 1; ...
    2579, 1; ...
    2595, 1; ...
    5390, 0; ...
    5534, 0; ...
    7038, 0; ...
    7140, 0; ...
    7205, 0; ...
    7354, 0];

NewSet = [
    2038, 1;
    2445, 1;
    2514, 1;
    2745.1, 1;
    2850, 1;
    2870, 1;
    % 5471, 0;
    7002, 0;
    % 7184, 0;
    % 7190, 0;
    7287, 0;
    7493, 1;
    2191, 1;
    2107, 1;
    2396, 1];

% Get ratings of all images
[~, ~, FullIAPS_allsubj_matrix] = getIAPSratings();

% pull selected images from full list
index_moreoptions = zeros(size(FullIAPS_allsubj_matrix,1),1);
for iImg = 1:size(MaiNeu,1)
    mai_subset_matrix(iImg,:) = [FullIAPS_allsubj_matrix( FullIAPS_allsubj_matrix(:,1) == MaiNeu(iImg,1) , : ), MaiNeu(iImg,2)];
    index_moreoptions = index_moreoptions + (FullIAPS_allsubj_matrix(:,1) == MaiNeu(iImg,1));
end
for iImg = 1:size(NewSet,1)
    new_subset_matrix(iImg,:) = [FullIAPS_allsubj_matrix( FullIAPS_allsubj_matrix(:,1) == NewSet(iImg,1) , : ), NewSet(iImg,2)];
    index_moreoptions = index_moreoptions + (FullIAPS_allsubj_matrix(:,1) == NewSet(iImg,1));
end

%{
%% code for dealing with multiple A/V entries for one image
for iList = 5 %1:length(FullList)
    for iSublist = 1 %:length(FullList{iList})
        clearvars tempList
        for iImage = 1:length(FullList{iList}{iSublist})
            tempRow = IAPSList_matrix( IAPSList_matrix(:,1) == FullList{iList}{iSublist}(iImage,1), :);
            if size(tempRow,1) > 1
                warning('multiple entries for List %d, Sublist %d, Image %d. Taking first entry\n',iList,iSublist,iImage);
                tempRow = tempRow(1,:);
            elseif size(tempRow,1) > 1
                error('no entries for List %d, Sublist %d, Image %d.\n',iList,iSublist,iImage);
            end
             tempList(iImage,:) = tempRow;
        end
        FullList{iList}{iSublist} = tempList;
    end
end
%}

%% If exporting the list of neutral images for the reapp task
% copy images to new folder
IAPS_neutral_subset = [mai_subset_matrix; new_subset_matrix];
makeIAPSSubsetFolder( IAPS_neutral_subset, 'subset_neutral', '~/Desktop' )
cd(fullfile('~/Desktop','subset_neutral'));
save('IAPS_neutral_subset.mat','IAPS_neutral_subset');


%% If looking for new images in the range of Mai's

mai_subset_matrix_range(1,:) = [min(mai_subset_matrix(:,2)), mean(mai_subset_matrix(:,2)), max(mai_subset_matrix(:,2))]; % val
mai_subset_matrix_range(2,:) = [min(mai_subset_matrix(:,4)), mean(mai_subset_matrix(:,4)), max(mai_subset_matrix(:,4))]; % arousal

idx = [~index_moreoptions, FullIAPS_allsubj_matrix(:,2) > mai_subset_matrix_range(1,1) , FullIAPS_allsubj_matrix(:,2) < mai_subset_matrix_range(1,3) , FullIAPS_allsubj_matrix(:,4) > mai_subset_matrix_range(2,1) , FullIAPS_allsubj_matrix(:,4) < mai_subset_matrix_range(2,3)];
idx2 = all(idx,2);
newoptions_subset_matrix = FullIAPS_allsubj_matrix(idx2,:);

% find other neutral options for EmoLoc and training
makeIAPSSubsetFolder( newoptions_subset_matrix, 'NeutralOptions', '~/Desktop' )

% 
% printListSummary( newoptions_subset_matrix, saveTitle, saveDir, paperResolution )
