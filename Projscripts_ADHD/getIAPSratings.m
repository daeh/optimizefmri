function [IAPSList_cell,IAPSList_struct,IAPSList_matrix] = getIAPSratings(dirPath,source,writeTextFile)
% Assymbles table of info about all IAPS images in the target directory

if nargin < 3
    writeTextFile = false;
end
if nargin == 1
    source = 'fresh';
elseif nargin < 1
    source = 'savedAllImages';
end

if strcmpi('savedAllImages',source)
    temp = load('/Volumes/cristae/samadhi_daeda/Research/Instruments/IAPS/dae_FullIAPS_allsubj_cell.mat'); % loads FullIAPS_allsubj cell array (duplicate values are stored as array under one IAPS ID, IDs are strings)
    IAPSList_cell = temp.FullIAPS_allsubj_cell;
    temp = load('/Volumes/cristae/samadhi_daeda/Research/Instruments/IAPS/dae_FullIAPS_allsubj_struct.mat'); % loads FullIAPS_allsubj_struct (duplicate values are stored as array under one IAPS ID, IDs are strings, structure with filepaths to photos)
    IAPSList_struct = temp.FullIAPS_allsubj_struct;
    temp = load('/Volumes/cristae/samadhi_daeda/Research/Instruments/IAPS/dae_FullIAPS_allsubj_matrix.mat'); % loads FullIAPS_allsubj_matrix (duplicate values are stored as array under duplicate IAPS ID, IDs are doubles)
    IAPSList_matrix = temp.FullIAPS_allsubj_matrix;
    clearvars temp
end

if strcmpi('fresh',source)
    cd(dirPath);
    
    IAPSList_struct = dir('*.jpg');
    for iFile = 1:size(IAPSList_struct,1)
        [~, IAPSList_struct(iFile).IAPS, ~] = fileparts(IAPSList_struct(iFile).name);
    end
    
    listHeaders = {'IAPS','V_mean','V_SD','A_mean','A_SD'};
    
    fileloc = '/Volumes/cristae/samadhi_daeda/Research/Instruments/IAPS/IAPS_1024px/AllSubjects_1-20.txt';
    fid = fopen(fileloc,'r');
    B = textscan(fid, '%s','Delimiter','\n');
    fclose(fid);
    B = B{:};
    B = B(8:end); % remove header information
    
    C = cellfun(@(s) strsplit(s, '\t'), B, 'UniformOutput', false);
    for iRow = 1:length(C)
        for iColumn = 1:length(C{iRow})
            D{iRow,iColumn} = C{iRow}{iColumn};
        end
    end
    
    iOutput = 1;
    for iFile = 1:size(IAPSList_struct,1)
        s = IAPSList_struct(iFile).IAPS;
        cellfind = @(s)(@(D)(strcmp(s,D)));
        logical_cells = cellfun(cellfind(IAPSList_struct(iFile).IAPS),D);
        %     logical_cells = ~cellfun(@isempty, strfind(D, temp_list(iFile).IAPS));
        
        indSelect = repmat(logical_cells(:,2),[1,size(logical_cells,2)]);
        indSelect(:,[1,7:end]) = 0; % remove all but ID, Vm, Vsd, Am, Asd
        
        temp_output = D(indSelect)';
        
        if sum(logical_cells(:)) > 1
            temp_output = reshape(temp_output,[length(temp_output)/5,5]);
        end
        
        for iRow = 1:size(temp_output,1)
            for iColumn = 2:size(temp_output,2)
                temp_ratings(iRow,iColumn-1) = str2num( temp_output{iRow,iColumn} );
            end
            for iColumn = 1:size(temp_output,2)
                IAPSList_matrix{iOutput,iColumn} = temp_output{iRow,iColumn};
            end
            iOutput = iOutput + 1;
        end
        IAPSList_struct(iFile).ValenceMean = temp_ratings(:,1);
        IAPSList_struct(iFile).ValenceSD = temp_ratings(:,2);
        IAPSList_struct(iFile).ArousalMean = temp_ratings(:,3);
        IAPSList_struct(iFile).ArousalSD = temp_ratings(:,4);
        clearvars temp_ratings temp_output logical_cells
    end
    clearvars -except IAPSList_cell IAPSList_struct IAPSList_matrix writeTextFile dirPath listHeaders
    
    % convert strings to doubles
    IAPSList_matrix = cellfun(@str2num, IAPSList_matrix);
    
    % put duplicate values into single cells
    IAPSList_cell = [{ IAPSList_struct.IAPS }', { IAPSList_struct.ValenceMean }', { IAPSList_struct.ValenceSD }', { IAPSList_struct.ArousalMean }', { IAPSList_struct.ArousalSD }'];
    
    if writeTextFile
        keyboard
        %%% use strjoin(C,',')
        fileID = fopen('output.txt','w');
        for iFile = 1:length(id2)
            fprintf(fileID,'%s\n',IAPSList_matrix{iFile,1});
        end
        fclose(fileID);
    end
    
end

end