function EmoLocList_make(runImmediately)

if nargin < 1
    runImmediately = false;
end

addpath('/Volumes/cristae/samadhi_daeda/coding/-GitRepos/OptimizeFMRI/Projscripts_ADHD');
saveDir = '/Volumes/cristae/samadhi_daeda/Desktop/LeftOut_Organized';

% get IAPS ratings for images in the leftout folders
[list_cell{1}, list_struct{1}, list_matrix{1}] = getIAPSratings('/Volumes/cristae/samadhi_daeda/coding/ADHD_EmotionRegulation/ReappraisalParadigm/development/IAPSStimFinal/SortedImageSets_InProcess/LeftOut_Organized/Meditators care less/Disgust');

[list_cell{2}, list_struct{2}, list_matrix{2}] = getIAPSratings('/Volumes/cristae/samadhi_daeda/coding/ADHD_EmotionRegulation/ReappraisalParadigm/development/IAPSStimFinal/SortedImageSets_InProcess/LeftOut_Organized/Meditators care less/Threat');

[list_cell{3}, list_struct{3}, list_matrix{3}] = getIAPSratings('/Volumes/cristae/samadhi_daeda/coding/ADHD_EmotionRegulation/ReappraisalParadigm/development/IAPSStimFinal/SortedImageSets_InProcess/LeftOut_Organized/Meditators care more/Disgust');

[list_cell{4}, list_struct{4}, list_matrix{4}] = getIAPSratings('/Volumes/cristae/samadhi_daeda/coding/ADHD_EmotionRegulation/ReappraisalParadigm/development/IAPSStimFinal/SortedImageSets_InProcess/LeftOut_Organized/Meditators care more/Threat');

for iList = 1:length(list_matrix)
    % remove duplicate values
    X = list_matrix{iList}(:,1);
    uniqueX = unique(X);
    countOfX = hist(X,uniqueX);
    indexToRepeatedValue = (countOfX~=1);
    idxRepVal = find(indexToRepeatedValue);
    
    if size(idxRepVal,2) > 0
        fprintf('\nList %d:\n',iList);
        repeatedValues = uniqueX(indexToRepeatedValue)
        numberOfAppearancesOfRepeatedValues = countOfX(indexToRepeatedValue)
        for iRepeat = length(idxRepVal):-1:1 % remove last images first so as to not screw up the indices
            idxRepValX = find(X == uniqueX(idxRepVal(iRepeat)))
            list_matrix{iList}( idxRepValX, :)
            warning('*** removing all entries after 1 ***')
            list_matrix{iList}( idxRepValX(2:end), :) = [];
        end
    end
    
    % sort the lists by increasing valence
    [list_matrix{iList}, temp_index] = sortrows(list_matrix{iList},[2 -4]);
    list_struct{iList} = list_struct{iList}(temp_index);
    list_cell{iList} = list_cell{iList}(temp_index,:);
    clearvars X uniqueX coutOfX indexToRepeatedValue idxRepVal repeatedValues numberOfAppearancesOfRepeatedValues idxRepValX
end

%% Export summaries of lists
for iList = 1:length(list_matrix)
    printListSummary( list_matrix{iList}, sprintf('EmoLocOptions_%d',iList), saveDir )
end


%% Define Lists
oblicatory = [list_matrix{1}([10],:); list_matrix{3}([46,50,62],:); list_matrix{4}([3,8,17],:)];
optional = [list_matrix{1}([1,6,11],:); list_matrix{2}([1:8,12,13],:); list_matrix{3}([1:16,18:30,32,34:45,47:49,51:61,63:66],:); list_matrix{4}([1,2,4:7,12,13,14,18],:)];
%{
1 SD
2 7 3
oblic
1 11

2 ST
1:8,12,13

3 OD
1:16,18:30,32,34:66, 64 66
46 50 62

4 OT
1:8,12,13,14,18
3 8 17
%}
combinedList = [oblicatory;sortrows(optional,2)];

listSize = 60;
%{
12 blocks * 5 unique images = 60
+ 1 target per block
12 block * 6 total = 66
%}
reducedList = combinedList(1:listSize,:);

if size(unique(reducedList(:,1)),1) ~= size(reducedList(:,1),1)
    error('Duplicates Found')
end

%% Balance runs
FullList = reducedList;
clearvars reducedList
if runImmediately
    findOptimizedListSequences('EmoLoc', FullList, 10, 50, BalancedLists, schedules, rng_currentstate, true);
else
    keyboard
    save('FullList','FullList');
end



end


