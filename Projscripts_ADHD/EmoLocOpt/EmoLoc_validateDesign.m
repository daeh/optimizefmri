function [store, tempBalancedList] = EmoLoc_validateDesign(Design)

% mainlist
[h(1,1),p(1,1)] = ttest( [ Design{1}(:,2); Design{2}(:,2); Design{3}(:,2)], [ Design{4}(:,2); Design{5}(:,2); Design{6}(:,2)]);
[h(1,2),p(1,2)] = ttest( [ Design{1}(:,4); Design{2}(:,4); Design{3}(:,4)], [ Design{4}(:,4); Design{5}(:,4); Design{6}(:,4)]);

[p(2,1),tbl,stats] = anova1([Design{1}(:,2); Design{2}(:,2); Design{3}(:,2); Design{4}(:,2); Design{5}(:,2); Design{6}(:,2)], kron([1:6], ones(1,10))', 'off' );
[p(3,1),tbl,stats] = anova1([Design{1}(:,2); Design{2}(:,2); Design{3}(:,2)], kron([1:3], ones(1,10))', 'off' );
[p(4,1),tbl,stats] = anova1([Design{4}(:,2); Design{5}(:,2); Design{6}(:,2)], kron([1:3], ones(1,10))', 'off' );

[p(2,2),tbl,stats] = anova1([Design{1}(:,4); Design{2}(:,4); Design{3}(:,4); Design{4}(:,4); Design{5}(:,4); Design{6}(:,4)], kron([1:6], ones(1,10))', 'off' );
[p(3,2),tbl,stats] = anova1([Design{1}(:,4); Design{2}(:,4); Design{3}(:,4)], kron([1:3], ones(1,10))', 'off' );
[p(4,2),tbl,stats] = anova1([Design{4}(:,4); Design{5}(:,4); Design{6}(:,4)], kron([1:3], ones(1,10))', 'off' );

tempBalancedList{1,1} = Design;
tempBalancedList{1,2} = p;
tempBalancedList{1, 3} = min(min(tempBalancedList{1, 2}));

    for iBlock = 1:size(Design,2)
        blockAVmean(iBlock,:) = mean(Design{iBlock}(:,[2,4]));
    end
    tempBalancedList{1,4} = blockAVmean;
    tempBalancedList{1, 5} = max( max(blockAVmean) - min(blockAVmean) ); % max difference in the means of the blocks

if tempBalancedList{1, 5} < .3 && min(min(p)) > .75
	store = true;
else
	store = false;
end

end