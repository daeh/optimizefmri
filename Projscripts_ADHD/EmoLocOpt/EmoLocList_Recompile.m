
% Sample balanced list options
addpath(genpath('/Volumes/cristae/samadhi_daeda/coding/-GitRepos/OptimizeFMRI/Projscripts_ADHD'))


% Load Reappraisal task images (neg and neutral)
temp_ReappList = load('/Volumes/cristae/samadhi_daeda/coding/ADHD_EmotionRegulation/ReappraisalParadigm/development/IAPSStimFinal/balanceSelectedLists_Run_reduced.mat');
ReappList = temp_ReappList.BalancedLists{1,1};
clearvars temp_*

% Load EmoLoc negative images
temp_EmoLocList = load('/Volumes/cristae/samadhi_daeda/coding/ADHD_EmotionRegulation/ReappraisalParadigm/development/IAPSStimFinal/EmoLoc_BalancedLists_Negative_reduced.mat');
EmoLocList = temp_EmoLocList.BalancedLists{1,1};
clearvars temp_*

saveDir = fullfile('~/Desktop/IAPS_Options');
saveVarDir = fullfile(saveDir,'Matlab_Variables');
mkdir(saveDir);
mkdir(saveVarDir);
% Copy full image set
copyfile('/Volumes/cristae/samadhi_daeda/Research/Instruments/IAPS/IAPS_1024px', saveDir);


[IAPSList_cell,IAPSList_struct,IAPSList_matrix] = getIAPSratings(saveDir);
% move Reapp images
newDirName = 'param_reapp';
newDirPath = fullfile(saveDir,newDirName);
mkdir(newDirPath);
for iRun = 1:size(ReappList,2)
    for iFile = 1:size(ReappList{iRun},1)
        struct_index = find(strcmp({IAPSList_struct.IAPS}, num2str(ReappList{iRun}(iFile,1)))==1);
        movefile(fullfile(IAPSList_struct(struct_index).folder, IAPSList_struct(struct_index).name),newDirPath)
    end
end
clearvars newDir* iRun iFile struct_index
% move EmoLoc images
newDirName = 'param_emoloc';
newDirPath = fullfile(saveDir,newDirName);
mkdir(newDirPath);
for iRun = 1:size(EmoLocList,2)
    for iFile = 1:size(EmoLocList{iRun},1)
        struct_index = find(strcmp({IAPSList_struct.IAPS}, num2str(EmoLocList{iRun}(iFile,1)))==1);
        movefile(fullfile(IAPSList_struct(struct_index).folder, IAPSList_struct(struct_index).name),newDirPath)
    end
end
clearvars newDir* iRun iFile struct_index
clearvars IAPSList_*
clearvars EmoLocList ReappList

%% Find neutral images (same code as FindNeuImages)
MaiNeu = [...
    2026, 1; ...
    2032, 1; ...
    2190, 1; ...
    2200, 1; ...
    2215, 1; ...
    2273, 1; ...
    2377, 1; ...
    2393, 1; ...
    2446, 1; ... % boots
    2480, 1; ...
    2512, 1; ...
    2570, 1; ...
    2579, 1; ...
    2595, 1; ...
    5390, 0; ...
    5534, 0; ...
    7038, 0; ...
    7140, 0; ...
    7205, 0; ...
    7354, 0];

[~, ~, FullIAPS_allsubj_matrix] = getIAPSratings();

% pull selected images from full list
index_moreoptions = zeros(size(FullIAPS_allsubj_matrix,1),1);
for iImg = 1:size(MaiNeu,1)
    mai_subset_matrix(iImg,:) = [FullIAPS_allsubj_matrix( FullIAPS_allsubj_matrix(:,1) == MaiNeu(iImg,1) , : ), MaiNeu(iImg,2)];
    index_moreoptions = index_moreoptions + (FullIAPS_allsubj_matrix(:,1) == MaiNeu(iImg,1));
end
clearvars FullIAPS_allsubj* iImg index_moreoptions
clearvars MaiNeu*

mai_subset_matrix_range(1,:) = [min(mai_subset_matrix(:,2)), mean(mai_subset_matrix(:,2)), max(mai_subset_matrix(:,2))]; % val
mai_subset_matrix_range(2,:) = [min(mai_subset_matrix(:,4)), mean(mai_subset_matrix(:,4)), max(mai_subset_matrix(:,4))]; % arousal

[~, Remaining_IAPS_struct, Remaining_IAPS_matrix] = getIAPSratings(saveDir);
idx = [Remaining_IAPS_matrix(:,2) > mai_subset_matrix_range(1,1) , Remaining_IAPS_matrix(:,2) < mai_subset_matrix_range(1,3) , Remaining_IAPS_matrix(:,4) > mai_subset_matrix_range(2,1) , Remaining_IAPS_matrix(:,4) < mai_subset_matrix_range(2,3)];
idx2 = all(idx,2);
newoptions_subset_matrix = Remaining_IAPS_matrix(idx2,:);
clearvars idx* mai_subset_matrix

% copy neutral EmoLoc Images Options
newDirName = 'param_emoloc_neuOptions_copies';
newDirPath = fullfile(saveDir,newDirName);
mkdir(newDirPath);
for iFile = 1:size(newoptions_subset_matrix,1)
    struct_index = find(strcmp({Remaining_IAPS_struct.IAPS}, num2str(newoptions_subset_matrix(iFile,1)))==1);
    copyfile(fullfile(Remaining_IAPS_struct(struct_index).folder, Remaining_IAPS_struct(struct_index).name),newDirPath)
end
clearvars Remaining_IAPS_* newoptions_subset_matrix iFile struct_index
keyboard; % Now, go delete images from that folder that you don't want included in the neutral options
[~, ~, FullList] = getIAPSratings(newDirPath);
rmdir(newDirPath, 's');
clearvars newDir*
%% Optimize neutral images for EmoBioLoc

save(fullfile(saveVarDir,'FullListNeutral.mat'),'FullList')
%%% Run sbatch
runOptimization = false;
if runOptimization
    
    %BalancedLists = findOptimizedListSequences('EmoLocNeu', FullList, 1000, 50);
else
    
end
clearvars FullList runOptimization

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Print EmoLoc Lists and select

runDir = fullfile(saveDir,sprintf('EmoLocRunOptions'));
mkdir(runDir);

temp_EmoLocList_Neu = load('/Volumes/cristae/samadhi_daeda/coding/ADHD_EmotionRegulation/ReappraisalParadigm/development/IAPSStimFinal/EmoLoc_BalancedLists_Neutral_reduced.mat');
    
% Print EmoLoc Neutral images
for iList = 2%1:10
    for iRun = 1:3
        printListSummary( temp_EmoLocList_Neu.BalancedLists{iList}{iRun}, sprintf('EmoLocListNeu%03d_Run1-%d',iList,iRun), runDir, [0 0 10 10] );
    end
    for iRun = 4:6
        printListSummary( temp_EmoLocList_Neu.BalancedLists{iList}{iRun}, sprintf('EmoLocListNeu%03d_Run2-%d',iList,iRun), runDir, [0 0 10 10] );
    end
end


temp_EmoLocList_Neg = load('/Volumes/cristae/samadhi_daeda/coding/ADHD_EmotionRegulation/ReappraisalParadigm/development/IAPSStimFinal/EmoLoc_BalancedLists_Negative_reduced.mat');

for iList = 1%1:10
    for iRun = 1:3
        printListSummary( temp_EmoLocList_Neg.BalancedLists{iList}{iRun}, sprintf('EmoLocListNeg%03d_Run1-%d',iList,iRun), runDir, [0 0 10 10] );
    end
    for iRun = 4:6
        printListSummary( temp_EmoLocList_Neg.BalancedLists{iList}{iRun}, sprintf('EmoLocListNeg%03d_Run2-%d',iList,iRun), runDir, [0 0 10 10] );
    end
end


EmoLocList_Neu = temp_EmoLocList_Neu.BalancedLists{2,1}; %%%%%%%%% Need to select list here
EmoLocList_Neg = temp_EmoLocList_Neg.BalancedLists{1,1}; %%%%%%%%% Need to select list here
clearvars temp_* runDir iRun iList

%% Assemble EmoLoc List
%%% set runs manually (images already randomized)
EmoLocListCombined{1}{1}{1} = EmoLocList_Neg{1};
EmoLocListCombined{1}{1}{2} = EmoLocList_Neg{2};
EmoLocListCombined{1}{1}{3} = EmoLocList_Neg{3};

EmoLocListCombined{1}{2}{1} = EmoLocList_Neu{1};
EmoLocListCombined{1}{2}{2} = EmoLocList_Neu{2};
EmoLocListCombined{1}{2}{3} = EmoLocList_Neu{3};


EmoLocListCombined{2}{1}{1} = EmoLocList_Neg{4};
EmoLocListCombined{2}{1}{2} = EmoLocList_Neg{5};
EmoLocListCombined{2}{1}{3} = EmoLocList_Neg{6};

EmoLocListCombined{2}{2}{1} = EmoLocList_Neu{4};
EmoLocListCombined{2}{2}{2} = EmoLocList_Neu{5};
EmoLocListCombined{2}{2}{3} = EmoLocList_Neu{6};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% move neutral EmoLoc Images (negs already moved)
[~, Remaining_IAPS_struct, ~] = getIAPSratings(saveDir);
newDirName = 'param_emoloc';
newDirPath = fullfile(saveDir,newDirName);
% mkdir(newDirPath);
for iRun = 1:size(EmoLocList_Neu,2)
    for iFile = 1:size(EmoLocList_Neu{iRun},1)
        struct_index = find(strcmp({Remaining_IAPS_struct.IAPS}, num2str(EmoLocList_Neu{iRun}(iFile,1)))==1);
        movefile(fullfile(Remaining_IAPS_struct(struct_index).folder, Remaining_IAPS_struct(struct_index).name),newDirPath)
    end
end
clearvars Remaining_IAPS_* newDir* iRun iFile struct_index
clearvars EmoLocList_*

save(fullfile(saveVarDir,'EmoLocListCombined.mat'),'EmoLocListCombined');
clearvars EmoLocListCombined


%% Find remaing images
% Find Neutral
[~, Remaining_IAPS_struct, Remaining_IAPS_matrix] = getIAPSratings(saveDir);
idx = [Remaining_IAPS_matrix(:,2) > mai_subset_matrix_range(1,1) , Remaining_IAPS_matrix(:,2) < mai_subset_matrix_range(1,3) , Remaining_IAPS_matrix(:,4) > mai_subset_matrix_range(2,1) , Remaining_IAPS_matrix(:,4) < mai_subset_matrix_range(2,3)];
idx2 = all(idx,2);
newoptions_subset_matrix = Remaining_IAPS_matrix(idx2,:);
clearvars idx*
% move images
NeutralOptionsDir = fullfile(saveDir,'AdditionalNeutralOptions');
mkdir(NeutralOptionsDir);
for iFile = 1:size(newoptions_subset_matrix,1)
    struct_index = find(strcmp({Remaining_IAPS_struct.IAPS}, num2str(newoptions_subset_matrix(iFile,1)))==1);
    movefile(fullfile(Remaining_IAPS_struct(struct_index).folder, Remaining_IAPS_struct(struct_index).name),NeutralOptionsDir)
end
clearvars Remaining_IAPS_* struct_index iFile NeutralOptionsDir newoptions_subset_matrix iFile
clearvars mai_subset_matrix_range

% Find Negative
[~, Remaining_IAPS_struct, Remaining_IAPS_matrix] = getIAPSratings(saveDir);
idx = [Remaining_IAPS_matrix(:,2) < 3, Remaining_IAPS_matrix(:,4) > 3];
idx2 = all(idx,2);
newoptions_subset_matrix = Remaining_IAPS_matrix(idx2,:);
clearvars idx*
% move images
newDirPath = fullfile(saveDir,'AdditionalNegativeOptions');
mkdir(newDirPath);
for iFile = 1:size(newoptions_subset_matrix,1)
    struct_index = find(strcmp({Remaining_IAPS_struct.IAPS}, num2str(newoptions_subset_matrix(iFile,1)))==1);
    movefile(fullfile(Remaining_IAPS_struct(struct_index).folder, Remaining_IAPS_struct(struct_index).name),newDirPath)
end
clearvars Remaining_IAPS_* struct_index newDirPath newoptions_subset_matrix iFile




