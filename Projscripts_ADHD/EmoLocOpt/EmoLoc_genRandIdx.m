function [newMapping, randidx] = EmoLoc_genRandIdx(MasterList)
newMapping = randperm(size(MasterList,1));
randidx = newMapping(:);
end