function [newDesign] = EmoLoc_buildDesign(MasterList, newMapping)

    newDesign{1} = MasterList(newMapping(1:10),:);
    newDesign{2} = MasterList(newMapping(11:20),:);
    newDesign{3} = MasterList(newMapping(21:30),:);
    newDesign{4} = MasterList(newMapping(31:40),:);
    newDesign{5} = MasterList(newMapping(41:50),:);
    newDesign{6} = MasterList(newMapping(51:60),:);

end