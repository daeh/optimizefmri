function [ output_args ] = printListSummary( IAPSList, saveTitle, saveDir, paperResolution )
%UNTITLED Prints summery sheets
%   Detailed explanation goes here

% IAPSList feed in as matrix of doubles

if nargin < 4
    paperResolution = [0 0 20 20];
end

addpath('/Volumes/cristae/samadhi_daeda/coding/-GitRepos/OptimizeFMRI/Projscripts_ADHD');
saveOptionSummaries = true;
%{
%%TEMP
IAPSList = BalancedLists{1, 1}{1, 1};
saveDir = '~/Desktop'
saveTitle = 'test1'
%%ENDTEMP
%}

% [FullIAPS_allsubj_cell, FullIAPS_allsubj_struct, FullIAPS_allsubj_matrix] = getIAPSratings('/Volumes/cristae/samadhi_daeda/Research/Instruments/IAPS/IAPS_1024px','fresh',false); % generates the variables from the IAPS text file
[FullIAPS_allsubj_cell, FullIAPS_allsubj_struct, FullIAPS_allsubj_matrix] = getIAPSratings(); % returns the pregenerated matlab variables

% find number of plots needed
nFile = size(IAPSList,1);
overflow = true; iRuns = 1; m = 1; n = 2;
while overflow
    if m*n < nFile && ~mod(iRuns,2)
        m = m + 1;
    elseif m*n < nFile && mod(iRuns,2)
        n = n + 1;
    else
        overflow = false;
    end
    iRuns = iRuns + 1;
end

if saveOptionSummaries
cd(saveDir);
    figure;
    set(gcf,'units','normalized','position',[0 0 1 1]);
    for iFile = 1:nFile
        % find selected IAPS image
        tf = strcmp(num2str(IAPSList(iFile)),{FullIAPS_allsubj_struct.IAPS});
image = imread(fullfile(FullIAPS_allsubj_struct(tf).folder,FullIAPS_allsubj_struct(tf).name));
        subplot(m,n,iFile)
        imshow(image)
        title(sprintf('%d:%s(%1.2f,%1.2f)', iFile, FullIAPS_allsubj_struct(tf).IAPS, FullIAPS_allsubj_struct(tf).ValenceMean, FullIAPS_allsubj_struct(tf).ArousalMean));
    end
    set(gcf,'PaperPosition',paperResolution)
    print(gcf,'-r300','-dpng',saveTitle)
end

close all



end

