% Used to generate STIM list of ADHA

clear;

% disgust self
ListFull{1} = [
    1270;
    1271;
    1274;
    1275;
    1280;
    1350;
    2730;
    7359;
    7360;
    7380;
    9290;
    9291;
    9295;
    9300;
    9301;
    9302;
    9320;
    9321;
    9322;
    9325;
    9326;
    9330;
    9340;
    9830;
    9832; ];

% disgust other
ListFull{2} = [
    2095;
    2352.2;
    2375.1;
    2661;
    2717;
    2718;
    2750;
    2981;
    3000;
    3001;
    3005.1;
    3010;
    3015;
    3016;
    3017;
    3030;
    3051;
    3053;
    3059;
    3060;
    3061;
    3062;
    3063;
    3064;
    3068;
    3069;
    3071;
    3080;
    3100;
    3101;
    3102;
    3103;
    3110;
    3120;
    3130;
    3131;
    3140;
    3150;
    3160;
    3168;
    3170;
    3185;
    % 3190;
    3191;
    3195;
    3211;
    3212;
    3213;
    3215;
    3220;
    3225;
    3261;
    3266;
    3350;
    3400;
    6022;
    8230;
    8231;
    9005;
    9006;
    9040;
    9043;
    9075;
    9140;
    9181;
    9182;
    9183;
    9185;
    9252;
    9253;
    9332;
    9341;
    9342;
    9400;
    9405;
    9410;
    9420;
    9433;
    9490;
    9500;
    9520;
    9561;
    9570;
    9571 ];

% threat self
ListFull{3} = [
    1022;
    1026;
    1030;
    1033;
    1040;
    1050;
    1051;
    1052;
    1070;
    1080;
    1090;
    1101;
    1110;
    1113;
    1114;
    1120;
    1200;
    1202;
    1205;
    1220;
    1230;
    1240;
    1300;
    1301;
    1302;
    1303;
    1304;
    1321;
    1525;
    1726;
    1820;
    1930;
    1931;
    1932;
    2681;
    2682;
    2811;
    6190;
    6200;
    6210;
    6230;
    6231;
    6242;
    6243;
    6244;
    6250.1;
    6250;
    6260;
    6263;
    6300;
    6314;
    6370;
    6510;
    9800 ];


% threat other
ListFull{4} = [
    1019;
    1112;
    1201;
    2345.1;
    2688;
    2694;
    3180;
    3181;
    3500;
    3530;
    6021;
    6211;
    6212;
    6312;
    6313;
    6315;
    6350;
    6360;
    6415;
    6520;
    6530;
    6540;
    6550;
    6560;
    6561;
    6562;
    6563;
    6570.1;
    6570;
    6571;
    6821;
    6832;
    6836;
    6840;
    8485;
    9041;
    9145;
    9150;
    9163;
    9265;
    9402;
    9403;
    9413;
    9414;
    9419;
    9423;
    9425;
    9426;
    9427;
    9428;
    9452;
    9635.1;
    9908];

% get V/A ratings for images
load('/Volumes/cristae/samadhi_daeda/coding/ADHD_EmotionRegulation/ReappraisalParadigm/development/IAPSStimFinal/IAPS_1024px/IAPS_SlideRatings_AllSubj.mat');
for iList = 1:length(ListFull)
    tempList = [];
    for iImage = 1:length(ListFull{iList})
        tempidx = IAPS_AllSub(:,1) == ListFull{iList}(iImage,1);
        tempList(end+1:end+sum(tempidx),:) = IAPS_AllSub(tempidx,:);
    end
    ListFull{iList} = tempList;
    clearvars tempList
    
    % get largest V other, smallest V self
    sortedV{iList} = sortrows(ListFull{iList},[2 -4]); % valence 0 = very negative, ++ = positive;
    
    %     fprintf('%d: V = %d, A = %d, n=%d\n', iList, mean(ListFull{iList}(:,2)), mean(ListFull{iList}(:,4)), size(ListFull{iList},1) );
end



%% Balance lists for 1-Self and 2-Other, each composed of 50% disgust and 50% threat

nStim = 16;
listSize = nStim*2; % 32 images in each list
index_upper = 1:nStim; % disgust
index_lower = nStim + 1 : listSize; % threat

%{
%% Very conservatively balanced image set. very well balanced, but I decided it was worth including more dramatic images and loosing some pvalues
% self disgust
idx1 = [1     2     3     4     5     6     8     9    10    11    12    13    14    15    16    17]; %1:16;
% took 7 out becuase it could cause meditators to have a sadness reaction, replaced with 16
tempList{1}(index_upper,:) = sortedV{1}(idx1,:);
% self threat
idx2 = [1     2     3     4     5     6     7     8     9    10    11    12    13    15    16    21]; %1:16;
%might sub 19/24 for 6
tempList{1}(index_lower,:) = sortedV{3}(idx2,:);

%{
find(sortedV{4}(:,1) == 6315)
%}
% other disgust
idx3 = [22     23     26    30     32     38     40     47     54     68     71     76     78     79     81    82]; %length(sortedV{2}) - 15 : length(sortedV{2});
% 12 is good
tempList{2}(index_upper,:) = sortedV{2}(idx3,:);
% other threat
idx4 = [1     3     7     8     9    11    15    22    25    30    31     33    36    38    40    48]; %length(sortedV{4}) - 15 : length(sortedV{4});
% would prefer to sub 4 for 31; replaced 45 with 36. Costs a bit, but the spider isn't a great OT
tempList{2}(index_lower,:) = sortedV{4}(idx4,:);
%}

% self disgust
idx1 = [1     2     3     4     5     6     8     9    10    11    12    13    14    15    16    17]; %1:16;
% took 7 out becuase it could cause meditators to have a sadness reaction, replaced with 16
tempList{1}(index_upper,:) = sortedV{1}(idx1,:);
% self threat
idx2 = [1     2     3     4     5     7     8     9    10    11    12    13    15    16    19    21]; %1:16;
%might sub 19/24 for 6
tempList{1}(index_lower,:) = sortedV{3}(idx2,:);

%{
9005 = 82
9043-67 9490-80 9520-63
find(sortedV{2}(:,1) == 9520)
sortedV{2}(find(sortedV{2}(:,1) == 9520),[2,4])
%}
% other disgust
idx3 = [22     23     26    30     32     38     40     47     54     68     71     76     78     79     80    81]; %length(sortedV{2}) - 15 : length(sortedV{2});
% 12 is good
tempList{2}(index_upper,:) = sortedV{2}(idx3,:);
% other threat
idx4 = [1     3     4    7     9    10    11    15    22    25    27    30    33    36    38    40]; %length(sortedV{4}) - 15 : length(sortedV{4});
% would prefer to sub 4 for 31; replaced 45 with 36. Costs a bit, but the spider isn't a great OT  38  40  48
tempList{2}(index_lower,:) = sortedV{4}(idx4,:);


for iList = 1:2
    if length(unique(tempList{iList}(:,1))) ~= length(tempList{iList}(:,1))
        [n, bin] = histc(tempList{iList}(:,1), unique(tempList{iList}(:,1)));
        multiple = find(n > 1);
        index    = find(ismember(bin, multiple));
        keyboard;
        tempList{iList}(index(1),1)
        
    end
end

run = true;
nBalanced = 1;


% while run
%% generate random list
%{
    % self disgust
    idx1 = randperm(size(ListFull{1},1));
    tempList{1}(index_upper,:) = ListFull{1}(idx1(1:nStim),:);
    % self threat
    idx3 = randperm(size(ListFull{3},1));
    tempList{1}(index_lower,:) = ListFull{3}(idx3(1:nStim),:);
    
    % other disgust
    idx2 = randperm(size(ListFull{2},1));
    tempList{2}(index_upper,:) = ListFull{2}(idx2(1:nStim),:);
    % other threat
    idx4 = randperm(size(ListFull{4},1));
    tempList{2}(index_lower,:) = ListFull{2}(idx4(1:nStim),:);
%}
%{
for iList1 = 1:length(tempList) - 1
    significant = false;
    for iList2 = iList1 + 1 : length(tempList)
        %             fprintf('%1.4f , %1.4f\n',mean(tempList{iList1}(:,2)) - mean(tempList{iList2}(:,2)), mean(tempList{iList1}(:,4)) - mean(tempList{iList2}(:,4)))
        
        % valence difference?
        [hV, pV] = ttest2(tempList{iList1}(:,2), tempList{iList2}(:,2));
        
        % arousal difference?
        [hA, pA] = ttest2(tempList{iList1}(:,4), tempList{iList2}(:,4));
        
        fprintf('Self vs Other: Valence: %1.4f, Arousal: %1.4f\n',pV,pA);
        % if either valence or arousal is significant, generate new lists
        if hV || hA
            significant = true;
            break;
        end
    end
    if significant
        break;
    end
end
%}

fprintf('Balanced List %d:\n',nBalanced);

fprintf('\n\np-values:\n')

% valence difference?
[hV, pV] = ttest2(tempList{1}(:,2), tempList{2}(:,2));

% arousal difference?
[hA, pA] = ttest2(tempList{1}(:,4), tempList{2}(:,4));

BalancedList{nBalanced,2} = [mean(tempList{1}(:,2)), mean(tempList{1}(:,4)); mean(tempList{2}(:,2)), mean(tempList{2}(:,4)); pV, pA]; % mean and p-values for whole lists
%         fprintf('Valence (p = %0.5f), Arousal (p = %0.5f)\n',pV, pA);
fprintf('Self vs Other: Valence: %1.4f, Arousal: %1.4f\n',pV,pA);

if hV || hA
    significant = true;
else
    significant = false;
end

% If lists are balanced, record results
if ~significant
    for iList = 1:length(tempList)
        BalancedList{nBalanced,1}{iList} = sortrows(tempList{iList},1);
    end
    
    
    % SD vs OD *
    [h(1,1), p(1,1)] = ttest2(tempList{1}(index_upper,2),tempList{2}(index_upper,2));
    [h(1,2), p(1,2)] = ttest2(tempList{1}(index_upper,4),tempList{2}(index_upper,4));
    % ST vs OT *
    [h(2,1), p(2,1)] = ttest2(tempList{1}(index_lower,2),tempList{2}(index_lower,2));
    [h(2,2), p(2,2)] = ttest2(tempList{1}(index_lower,4),tempList{2}(index_lower,4));
    
    % SD vs ST *
    [h(3,1), p(3,1)] = ttest2(tempList{1}(index_upper,2),tempList{1}(index_lower,2));
    [h(3,2), p(3,2)] = ttest2(tempList{1}(index_upper,4),tempList{1}(index_lower,4));
    % OD vs OT *
    [h(4,1), p(4,1)] = ttest2(tempList{2}(index_upper,2),tempList{2}(index_lower,2));
    [h(4,2), p(4,2)] = ttest2(tempList{2}(index_upper,4),tempList{2}(index_lower,4));
    
    % SD vs OT
    [h(5,1), p(5,1)] = ttest2(tempList{1}(index_upper,2),tempList{2}(index_lower,2));
    [h(5,2), p(5,2)] = ttest2(tempList{1}(index_upper,4),tempList{2}(index_lower,4));
    % OD vs ST
    [h(6,1), p(6,1)] = ttest2(tempList{2}(index_upper,2),tempList{1}(index_lower,2));
    [h(6,2), p(6,2)] = ttest2(tempList{2}(index_upper,4),tempList{1}(index_lower,4));
    
    BalancedList{nBalanced,3} = p;
    
    names = {'SD vs OD', 'ST vs OT', 'SD vs ST', 'OD vs OT', 'SD vs OT', 'OD vs ST'};
    for iP = 1:size(p,1)
        fprintf('%s: Valence: %0.4f, Arousal: %0.4f\n',names{iP}, p(iP,1), p(iP,2))
    end
    
    fprintf('\n');
    
end

fprintf('Self          mean : (V=%1.3f, A=%1.3f)\n', mean(tempList{1}(:,2)), mean(tempList{1}(:,4)));
fprintf('Other         mean : (V=%1.3f, A=%1.3f)\n', mean(tempList{2}(:,2)), mean(tempList{2}(:,4)));
fprintf('Difference    mean : (V=%1.3f, A=%1.3f)\n', mean(tempList{2}(:,2)) - mean(tempList{1}(:,2)), mean(tempList{2}(:,4)) - mean(tempList{1}(:,4)));
fprintf('Self-Disgust  means: (V=%1.3f, A=%1.3f)\n', mean(tempList{1}(index_upper,2)), mean(tempList{1}(index_upper,4)));
fprintf('Self-Threat   means: (V=%1.3f, A=%1.3f)\n', mean(tempList{1}(index_lower,2)), mean(tempList{1}(index_lower,4)));
fprintf('Other-Disgust means: (V=%1.3f, A=%1.3f)\n', mean(tempList{2}(index_upper,2)), mean(tempList{2}(index_upper,4)));
fprintf('Other-Threat  means: (V=%1.3f, A=%1.3f)\n', mean(tempList{2}(index_lower,2)), mean(tempList{2}(index_lower,4)));

fprintf('\n\n');

% end
keyboard;
%% copy files
sortedIAPS = '/Volumes/cristae/samadhi_daeda/coding/ADHD_EmotionRegulation/ReappraisalParadigm/development/IAPSStimFinal/IAPS_1024px';
studyOptions = fullfile(sortedIAPS,'StudyOptions');
leftOut = fullfile(sortedIAPS,'LeftOut');
mkdir(leftOut);
copyfile(studyOptions,leftOut);

mkdir(fullfile(leftOut,'subset_SD'));
mkdir(fullfile(leftOut,'subset_ST'));
mkdir(fullfile(leftOut,'subset_OD'));
mkdir(fullfile(leftOut,'subset_OT'));


iList = 1;
cd(leftOut)
tempDir = dir('SD*');
cd(tempDir.name);
for iPic = 1:16
    movefile(sprintf('%s.jpg',num2str(tempList{iList}(iPic,1))),'../subset_SD');
end
cd(leftOut)
tempDir = dir('ST*');
cd(tempDir.name);
for iPic = 17:32
    movefile(sprintf('%s.jpg',num2str(tempList{iList}(iPic,1))),'../subset_ST');
end

iList = 2;
cd(leftOut)
tempDir = dir('OD*');
cd(tempDir.name);
for iPic = 1:16
    movefile(sprintf('%s.jpg',num2str(tempList{iList}(iPic,1))),'../subset_OD');
end
cd(leftOut)
tempDir = dir('OT*');
cd(tempDir.name);
for iPic = 17:32
    movefile(sprintf('%s.jpg',num2str(tempList{iList}(iPic,1))),'../subset_OT');
end

%% Print Lists
for ii = 1:2 % self, other x (disgust, threat)
    fprintf('IAPS\tValence Mean\tArousal Mean\n')
    for kk = 1:32 % images
        fprintf('%s\t%1.4f\t%1.4f\n',num2str(tempList{ii}(kk,1)),tempList{ii}(kk,2),tempList{ii}(kk,3))
        if kk == 16
            fprintf('\n')
        end
    end
    fprintf('\n')
end


%% Next use balanceSelectedLists_Set(tempList,3000000)
