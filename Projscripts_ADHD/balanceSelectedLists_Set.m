function [BalancedLists] = balanceSelectedLists_Set(tempList,nDesigns)

rngseed = 'default';

%% Assign human codes to photos
human = [
    1202, 0;
    2688, 1;
    2718, 1;
    2730, 1;
    2750, 1;
    2811, 1;
    3030, 1;
    3059, 1;
    3060, 1;
    3069, 1;
    3150, 1;
    3195, 1;
    3261, 1;
    3350, 1;
    3500, 1;
    3530, 1;
    6200, 1;
    6210, 1;
    6230, 1;
    6231, 1;
    6242, 1;
    6243, 1;
    6250.1, 1;
    6260, 1;
    6263, 1;
    6300, 1;
    6312, 1;
    6313, 1;
    6350, 1;
    6370, 1;
    6510, 1;
    6520, 1;
    6550, 1;
    6560, 1;
    6840, 1;
    8230, 1;
    9005, 1;
    9163, 1;
    9321, 1;
    9322, 1;
    9325, 1;
    9326, 1;
    9330, 1;
    9341, 1;
    9413, 1;
    9414, 1;
    9425, 1;
    9426, 1;
    9427, 1;
    9428, 1;
    9452, 1;
    9800, 1;
    1050, 0;
    1525, 0;
    7359, 0;
    7380, 0;
    9182, 0;
    9183, 0;
    9290, 0;
    9291, 0;
    9300, 0;
    9301, 0;
    9302, 0;
    9320, 0;
    9340, 0;
    9490, 1;
    9419, 1;
    9561, 0;
    9830, 0];

% Asign human codes to items
for iList = 1:length(tempList)
    for iImage = 1:size(tempList{iList},1)
        % determine if there are any missing human codes
        if length(find(tempList{iList}(iImage,1) == human(:,1))) ~= 1
            tempList{iList}(iImage,1)
        end
        tempList{iList}(iImage,6) = human(find(tempList{iList}(iImage,1) == human(:,1)),2);
    end
    clearvars iImage
end
clearvars iList

equalHumans = floor( ( sum( tempList{1}(:,6) ,1 ) + sum( tempList{2}(:,6) ,1 ) ) / length(tempList) );

% SD
FullList{1} = tempList{1}(1:16,:);
% ST
FullList{2} = tempList{1}(17:32,:);
% OD
FullList{3} = tempList{2}(1:16,:);
% OT
FullList{4} = tempList{2}(17:32,:);

idx_sd = 1:8;
idx_st = 9:16;
idx_od = 17:24;
idx_ot = 25:32;

%% Balance Lists
nListsStored = 50;
rndidx = [randperm(16); randperm(16); randperm(16); randperm(16)];
temp_schedule = rndidx(:);
schedules = single(NaN(size(temp_schedule,1),nDesigns));
iDesign = 1;
iStoredDesigns = 1;
uniqueDesignsDetermined = 0;
while iDesign <= nDesigns
    clearvars rndidx list h p humansPresent
    
    runImmediately = true; % generate first schedule before checking for uniqueness
    while runImmediately || any( all( bsxfun( @eq, schedules, single(temp_schedule) ), 1) )
        runImmediately = false;
        rndidx = [randperm(16); randperm(16); randperm(16); randperm(16)];
        temp_schedule = rndidx(:);
    end
    schedules(:,iDesign) = temp_schedule;
    iDesign = iDesign + 1;
    
    % generate design from schedule
    list = buildDesign(FullList, rndidx, idx_sd,idx_st,idx_od,idx_ot);
    
    % first validation: presence of humans balanced?
    humansPresent = sum([list{1}(:,6), list{2}(:,6)]);
    if ~any(equalHumans-1 > humansPresent)  % chance to avoid doing the following calculations if the simple test fails
        
        % second validation
        [h,p] = validateDesign(list, idx_sd,idx_st,idx_od,idx_ot);
        
        
        % if design passes validation, store it
        if ~any(h(:)) %% This is done above: && any(equalHumans == sum(humansPresent,1))
            BalancedLists{iStoredDesigns,1} = list;
            BalancedLists{iStoredDesigns,2} = p;
            BalancedLists{iStoredDesigns,3} = min(p(:));
            BalancedLists{iStoredDesigns,4} = humansPresent;
            
            iStoredDesigns = iStoredDesigns + 1;
            uniqueDesignsDetermined = uniqueDesignsDetermined + 1;
            
        end
        
    end
    
    if (~mod(iDesign,100000) && iDesign >= 200000) || (~mod(iDesign,10000) && iDesign < 200000 && iDesign >= 10000) || (~mod(iDesign,1000) && iDesign < 10000)
        fprintf('(%d unique designs, %d validated)  ',iDesign,uniqueDesignsDetermined);
        [pval_top, ~] = sort([BalancedLists{:,3}], 'descend');
        fprintf('max-min p-value = %0.4f\n',pval_top(1))
        clearvars pval_top
    end
    if ~mod(iDesign,100000)
        % trim down stored designs
        if iStoredDesigns > nListsStored + 1000
            BalancedLists = sortAndTruncateStoredDesigns(BalancedLists,nListsStored);
            iStoredDesigns = nListsStored + 1;
        end
        rng_currentstate = rng;
        save('intermediateWS.mat')
        fprintf('--intermediate workspace saved--\n');
    end
end

BalancedLists = sortAndTruncateStoredDesigns(BalancedLists,nListsStored);

clearvars -except tempList BalancedLists schedules uniqueDesignsDetermined

%%

save('balanceSelectedLists_Set.mat')
%{
for iList = 4
    fprintf('\tSelf vs Other p-values\n')
    fprintf('\tValence\tArousal\n')
    fprintf('All\t%1.4f\t%1.4f\n',BalancedLists{iList,2}(1,1),BalancedLists{iList,2}(1,2));
    fprintf('SD\t%1.4f\t%1.4f\n',BalancedLists{iList,2}(2,1),BalancedLists{iList,2}(2,2));
    fprintf('ST\t%1.4f\t%1.4f\n',BalancedLists{iList,2}(3,1),BalancedLists{iList,2}(3,2));
    fprintf('OD\t%1.4f\t%1.4f\n',BalancedLists{iList,2}(4,1),BalancedLists{iList,2}(4,2));
    fprintf('OT\t%1.4f\t%1.4f\n',BalancedLists{iList,2}(5,1),BalancedLists{iList,2}(5,2));
end
%}

end


function [list] = buildDesign(FullList, rndidx, idx_sd,idx_st,idx_od,idx_ot)

list{1}(idx_sd,:) = FullList{1}(rndidx(1,1:8),:);
list{2}(idx_sd,:) = FullList{1}(rndidx(1,9:16),:);

list{1}(idx_st,:) = FullList{2}(rndidx(2,1:8),:);
list{2}(idx_st,:) = FullList{2}(rndidx(2,9:16),:);

list{1}(idx_od,:) = FullList{3}(rndidx(3,1:8),:);
list{2}(idx_od,:) = FullList{3}(rndidx(3,9:16),:);

list{1}(idx_ot,:) = FullList{4}(rndidx(4,1:8),:);
list{2}(idx_ot,:) = FullList{4}(rndidx(4,9:16),:);

end


function [h,p] = validateDesign(list,idx_sd,idx_st,idx_od,idx_ot)

% mainlist
[h(1,1),p(1,1)] = ttest(list{1}(:,2), list{2}(:,2));
[h(1,2),p(1,2)] = ttest(list{1}(:,4), list{2}(:,4));

% SD
[h(2,1),p(2,1)] = ttest(list{1}(idx_sd,2), list{2}(idx_sd,2));
[h(2,2),p(2,2)] = ttest(list{1}(idx_sd,4), list{2}(idx_sd,4));

% ST
[h(3,1),p(3,1)] = ttest(list{1}(idx_st,2), list{2}(idx_st,2));
[h(3,2),p(3,2)] = ttest(list{1}(idx_st,4), list{2}(idx_st,4));

% OD
[h(4,1),p(4,1)] = ttest(list{1}(idx_od,2), list{2}(idx_od,2));
[h(4,2),p(4,2)] = ttest(list{1}(idx_od,4), list{2}(idx_od,4));

% OT
[h(5,1),p(5,1)] = ttest(list{1}(idx_ot,2), list{2}(idx_ot,2));
[h(5,2),p(5,2)] = ttest(list{1}(idx_ot,4), list{2}(idx_ot,4));

end


function [BalancedLists] = sortAndTruncateStoredDesigns(BalancedLists,nListsStored)

[~, sortidx] = sort([BalancedLists{:,3}], 'descend');
BalancedLists = BalancedLists(sortidx,:);
if size(BalancedLists,1) < nListsStored
    nListsStored = size(BalancedLists,1);
end
BalancedLists = BalancedLists(1:nListsStored,:);

end

