function [ output_args ] = ADHD_Dore4_CollectResults_swapInstructions( savedir )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%% This keeps the scheudle exactly the same and swaps the images in set_a with set_b. Alternatively, could swap the instructions and keep the image order the same


cd(savedir);

%% Get images from balanceSelectedLists_Run.m
load('/Volumes/cristae/samadhi_daeda/coding/ADHD_EmotionRegulation/ReappraisalParadigm/development/IAPSStimFinal/balanceSelectedLists_Run_reduced.mat');
clearvars -except BalancedLists
SelectedRunsList = 1; % which balanced runs list to use
%%% temp, should come from balanceSelectedLists_Run.m
clearvars idx
%{
% Reappraise set b
idx{1} = 1:2; %SD
idx{2} = 3:4; %ST
idx{3} = 5:6; %OD
idx{4} = 7:8; %OT
% View set b
idx{5} = 9:10; %SD
idx{6} = 11:12; %ST
idx{7} = 13:14; %OD
idx{8} = 15:16; %OT
% Neutral
idx{9} = 17:24; %Neu
%}

% Neg Stim Set A
idx{1} = 1:4; %Self D & T
idx{2} = 5:8; %Other D & T
% Neg Stim Set B
idx{3} = 9:12; %Self D & T
idx{4} = 13:16; %Other D & T
% Neutral
idx{5} = 17:24; %Neu


% Give each stimulus group a unique identifier
for iList = 1:size(BalancedLists,1)
    for iRun = 1:numel(BalancedLists{iList})
        BalancedLists{iList}{iRun} = [BalancedLists{iList}{iRun}, [1;1;2;2;3;3;4;4;5;5;6;6;7;7;8;8;9;9;9;9;9;9;9;9]]; % add stimulus class identifier
    end
end

for iList = 1:size(BalancedLists,1)
    for iRun = 1:numel(BalancedLists{iList})
        for iIdx = 1:numel(idx)
            BalancedListsStimGrouped{iList,1}{iRun,1}{iIdx,1} = BalancedLists{iList}{iRun}(idx{iIdx},[1,9]); % column 1 is stimulus, 9 is class identifier
        end
    end
end

%% shuffle stimuli within group
for iList = 1:size(BalancedLists,1)
    for iRun = 1:numel(BalancedLists{iList})
        for iIdx = 1:numel(idx)
            if iRun == 1 % so that every run has same order of stim
                temp_idx{iIdx} = randperm(size(BalancedListsStimGrouped{iList,1}{iRun,1}{iIdx,1},1));
            end
            BalancedListsStimGrouped{iList,1}{iRun,1}{iIdx,1} = BalancedListsStimGrouped{iList,1}{iRun,1}{iIdx,1}(temp_idx{iIdx},:);
        end
    end
end
%{
for iList = 1:size(BalancedLists,1)
    for iRun = 1:numel(BalancedLists{iList})
        for iIdx = 1:numel(idx)
            temp_idx = randperm(size(BalancedListsStimGrouped{iList,1}{iRun,1}{iIdx,1},1));
            BalancedListsStimGrouped{iList,1}{iRun,1}{iIdx,1} = BalancedListsStimGrouped{iList,1}{iRun,1}{iIdx,1}(temp_idx,:);
        end
    end
end
%}
%%% end temp


ImageList = BalancedListsStimGrouped{SelectedRunsList, 1}; % Select which optimized designs to use
clearvars -except ImageList



%% Get Selected optimized designs
load('/Volumes/cristae/samadhi_daeda/coding/ADHD_EmotionRegulation/ReappraisalParadigm/development/IAPSStimFinal/dore4_hrf_2m_resumed_20170219.mat'); % loads Cfg variable
clearvars -except Cfg ImageList
selectedSchedule = [1, 24, 31, 85]; % which inflated schedules to use

nRuns = length(selectedSchedule);

%% Deflate optimized designs, merge with selected images, and turn into spreadsheats
for iInflatedSchedule = 1:nRuns % each inflated schedule is treated as a RUN
    
    temp_sched1 = Cfg{1, 1}.InflatedSchedules{selectedSchedule(iInflatedSchedule)}; %get inflated schedule
    if max(sum(temp_sched1,2)) > 1
        error('overlapping conditions')
    end
    temp_sched2 = temp_sched1.*repmat([1 2 3 4 5 6 7],[size(temp_sched1,1),1]); %columns identified
    temp_sched3 = sum(temp_sched2,2); %single column
    deflatedSchedule = temp_sched3(1:10:end); %deflated schedule  ** could also use dae_downsample.m
    singleColumnSchedules{iInflatedSchedule} = deflatedSchedule;
    %{
    % make set_b
    temp_sched2 = temp_sched1.*repmat([1 2 3 4 5 6 7],[size(temp_sched1,1),1]); %columns identified
    temp_sched3 = sum(temp_sched2,2); %single column
    deflatedSchedule = temp_sched3(1:10:end); %deflated schedule  ** could also use dae_downsample.m
    singleColumnSchedules{1}{iInflatedSchedule} = deflatedSchedule;
    % make set_a
    temp_sched2 = temp_sched1.*repmat([3 4 1 2 5 6 7],[size(temp_sched1,1),1]); %columns identified
    temp_sched3 = sum(temp_sched2,2); %single column
    deflatedSchedule = temp_sched3(1:10:end); %deflated schedule  ** could also use dae_downsample.m
    singleColumnSchedules{2}{iInflatedSchedule} = deflatedSchedule;
    %}
    clearvars temp_sched*
    
    %get ISI and ITI
    rests = ~deflatedSchedule;
    rest_sequence = 0;
    secs = 0;
    iRests = 1;
    for idx = 2:length(rests)
        if rests(idx)
            secs = secs + 1;
        else
            if rests(idx-1)
                rest_sequence(iRests,1) = secs;
                iRests = iRests + 1;
                secs = 0;
            end
        end
    end
    ISI_ITI = [rest_sequence(2:2:end),[2;rest_sequence(3:2:end)]]; % INSERT INITIAL OR TERMINAL ISIB
    clearvars secs iRests rest_sequence idx
    
    %get instructions
    instr_sequence = {''};
    iIsr = 1;
    for idx = 2:length(rests)
        if deflatedSchedule(idx) == 1 || deflatedSchedule(idx) == 2 % reappraise
            if deflatedSchedule(idx-1) ~= deflatedSchedule(idx)
                instr_sequence{iIsr,1} = 'instructions/reappraise.png';
                iIsr = iIsr + 1;
            end
        elseif deflatedSchedule(idx) == 3 || deflatedSchedule(idx) == 4 || deflatedSchedule(idx) == 5 % view
            if deflatedSchedule(idx-1) ~= deflatedSchedule(idx)
                instr_sequence{iIsr,1} = 'instructions/attend.png';
                iIsr = iIsr + 1;
            end
        end
    end
    clearvars iIsr idx
    
    
    %% make sure that images reappraised for one set of subjects are viewed by an other
    %% Stim set A
    stimDict{1}(1,:) = [1,1]; % deflatedSchedule Identifier -> ImageList cell
    stimDict{1}(2,:) = [2,2];
    stimDict{1}(3,:) = [3,3];
    stimDict{1}(4,:) = [4,4];
    stimDict{1}(5,:) = [5,5];
    
    %% Stim set B
    stimDict{2}(1,:) = [1,5]; % deflatedSchedule Identifier -> ImageList cell
    stimDict{2}(2,:) = [2,6];
    stimDict{2}(3,:) = [3,7];
    stimDict{2}(4,:) = [4,8];
    stimDict{2}(5,:) = [9,9];
    
    
    
    temp_ImageList = ImageList; % reassign so that stim can be deleted once used
    iStimSet = 1;
    
    %get IAPS type
    stimuli_sequence{iStimSet} = {''};
    iIsr = 1;
    for idx = 2:length(rests)
        if ismember(deflatedSchedule(idx),stimDict{iStimSet}(:,1))
            if deflatedSchedule(idx-1) ~= deflatedSchedule(idx)
                %  The stim were randomized within group above, so this is no longer needed:  tempStimIdx = randi(size(ImageList{iInflatedSchedule}{stimDict(deflatedSchedule(idx),2)},1)); % get a random stimulus index from the stim category
                tempStimIdx = 1; % take first stimulus
                stimuli_sequence{iStimSet}{iIsr,1} = sprintf('stimuli/%s.jpg',num2str( temp_ImageList{iInflatedSchedule}{ stimDict{iStimSet}( deflatedSchedule(idx) ,2 ) }(tempStimIdx,1)) );
                stimuli_identification{iStimSet}(iIsr,1) = temp_ImageList{iInflatedSchedule}{stimDict{iStimSet}(deflatedSchedule(idx),2)}(tempStimIdx,2);
                temp_ImageList{iInflatedSchedule}{stimDict{iStimSet}(deflatedSchedule(idx),2)}(tempStimIdx,:) = []; % delete that stimulus;
                iIsr = iIsr + 1;
            end
        end
    end
    
    
    %% Make second Stim Set by swapping stim corresponding to columns of stimDict{2}
    for iClass = 1:size(stimDict{2},1)
        
        temp_idx1 = stimuli_identification{iStimSet} == stimDict{2}(iClass,1);
        temp_idx2 = stimuli_identification{iStimSet} == stimDict{2}(iClass,2);
        
        stimuli_sequence{2}(temp_idx1,:) = stimuli_sequence{iStimSet}(temp_idx2,:);
        stimuli_sequence{2}(temp_idx2,:) = stimuli_sequence{iStimSet}(temp_idx1,:);
        
        stimuli_identification{2}(temp_idx1,:) = stimuli_identification{iStimSet}(temp_idx2,:);
        stimuli_identification{2}(temp_idx2,:) = stimuli_identification{iStimSet}(temp_idx1,:);
    end
    
    
    for iStimSet = 1:2
        for iRow = 1:length(instr_sequence)
            scheduleTable{iStimSet}{iInflatedSchedule,1}{iRow,1} = ISI_ITI(iRow,2);
            scheduleTable{iStimSet}{iInflatedSchedule,1}{iRow,2} = instr_sequence{iRow};
            scheduleTable{iStimSet}{iInflatedSchedule,1}{iRow,3} = stimuli_sequence{iStimSet}{iRow};
            scheduleTable{iStimSet}{iInflatedSchedule,1}{iRow,4} = ISI_ITI(iRow,1);
            scheduleTable{iStimSet}{iInflatedSchedule,1}{iRow,5} = stimuli_identification{iStimSet}(iRow,1);
        end
    end
    
    %% Make second Stim Set by swapping instructions for neg set_a and set_b
    %%% ths is hacked together from *_swapImages
    scheduleTable{2} = scheduleTable{1};
    for iRun = 1:size(scheduleTable{2},1)
        for iRow = 1:size(scheduleTable{2}{iRun},1)
            if any(scheduleTable{2}{iRun}{iRow,5} == [1:4]) % column 5 is stim ID
                scheduleTable{2}{iRun}{iRow,2} = 'instructions/attend.png'; % column 2 is instructions
            elseif any(scheduleTable{2}{iRun}{iRow,5} == [5:8]) % column 5 is stim ID
                scheduleTable{2}{iRun}{iRow,2} = 'instructions/reappraise.png'; % column 2 is instructions
            end
        end
    end
    
    
    clearvars -except scheduleTable iInflatedSchedule Cfg ImageList selectedSchedules nRuns singleColumnSchedules selectedSchedule
end

clearvars -except scheduleTable

%% Write
mkdir('parameters');
cd('parameters');
stimSetPrefix = {'a','b'};
SPM_labels = {'Reapp_SD','Reapp_ST','Reapp_OD','Reapp_OT','Attend_SD','Attend_ST','Attend_OD','Attend_OT','Attend_Neutral','Instructions','Response'};
for iStimSet = 1:numel(scheduleTable)
    for iRun = 1:numel(scheduleTable{iStimSet}) % previously iInflatedSchedule
        fileID = fopen(sprintf('param_%s%d.csv',stimSetPrefix{iStimSet},iRun),'w');
        fprintf(fileID,'ISIB,instructions_image,IAPS_image,ISIA,stimClass,stimClassDesc\n');
        for iRow = 1:size(scheduleTable{iStimSet}{iRun},1)
            
            if iStimSet == 1 %stimset A
                if scheduleTable{iStimSet}{iRun,1}{iRow,5} == 1
                    stimulusClassDesc = 'Reapp_SD';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 2
                    stimulusClassDesc = 'Reapp_ST';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 3
                    stimulusClassDesc = 'Reapp_OD';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 4
                    stimulusClassDesc = 'Reapp_OT';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 5
                    stimulusClassDesc = 'Attend_SD';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 6
                    stimulusClassDesc = 'Attend_ST';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 7
                    stimulusClassDesc = 'Attend_OD';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 8
                    stimulusClassDesc = 'Attend_OT';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 9
                    stimulusClassDesc = 'Attend_Neutral';
                else
                    error('not recognized')
                end
            elseif iStimSet == 2 %stimset B
                if scheduleTable{iStimSet}{iRun,1}{iRow,5} == 5
                    stimulusClassDesc = 'Reapp_SD';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 6
                    stimulusClassDesc = 'Reapp_ST';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 7
                    stimulusClassDesc = 'Reapp_OD';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 8
                    stimulusClassDesc = 'Reapp_OT';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 1
                    stimulusClassDesc = 'Attend_SD';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 2
                    stimulusClassDesc = 'Attend_ST';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 3
                    stimulusClassDesc = 'Attend_OD';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 4
                    stimulusClassDesc = 'Attend_OT';
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 9
                    stimulusClassDesc = 'Attend_Neutral';
                else
                    error('not recognized')
                end
            else
                error('this section of code is specific for 2 couterbalanced lists')
            end
            
            fprintf(fileID,sprintf('%d,%s,%s,%d,%d,%s\n',scheduleTable{iStimSet}{iRun,1}{iRow,:}, stimulusClassDesc));
        end
        fclose(fileID);
        
        clearvars fileID iRow stimulusClassDesc
        
        %% Make SPM design matrix, onsets. Same for both randomizations, but this code produces two sets
        %    SPM_labels = {'Reapp_SD','Reapp_ST','Reapp_OD','Reapp_OT','Attend_SD','Attend_ST','Attend_OD','Attend_OT','Attend_Neutral','Instructions','Response'};
        
        
        DesignMatrix_time = 496;
        DesignMatrix_classes = 6;
        DesignMatrix = NaN(DesignMatrix_time,DesignMatrix_classes);
        DesignMatrix(1:6,:) = 0;
        %         DesignMatrixIDs
        
        iTime = 7;
        for iRow = 1:size(scheduleTable{iStimSet}{iRun},1) % rows of spreadsheet fed to psychopy
            % 1) ISIB
            block_duration = scheduleTable{iStimSet}{iRun}{iRow,1};
            block_design = zeros( block_duration, DesignMatrix_classes );
            DesignMatrix( iTime : iTime + block_duration -1, : ) = block_design;
            iTime = iTime + block_duration;
            clearvars block_*
            
            % 2) INSTR
            block_duration = 2;
            block_design = zeros( block_duration, DesignMatrix_classes );
            if strcmpi( scheduleTable{iStimSet}{iRun}{iRow,2}, 'instructions/reappraise.png')  %%DEBUG SPELLING
                block_input = 1;
            elseif strcmpi( scheduleTable{iStimSet}{iRun}{iRow,2}, 'instructions/attend.png')
                block_input = 2;
            else
                error('unknown')
            end
            block_design(:,2) = block_input; % set input column
            DesignMatrix( iTime : iTime + block_duration -1, : ) = block_design;
            iTime = iTime + block_duration;
            clearvars block_*
            
            % 3) IAPS
            block_duration = 8;
            block_design = zeros( block_duration, DesignMatrix_classes );
            if iStimSet == 1 %stimset A
                if scheduleTable{iStimSet}{iRun,1}{iRow,5} == 1
                    %                     stimulusClassDesc = 'Reapp_SD';
                    block_imageClass = 1;
                    block_task = 1;
                    block_imageXtask = 1;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 2
                    %                     stimulusClassDesc = 'Reapp_ST';
                    block_imageClass = 2;
                    block_task = 1;
                    block_imageXtask = 2;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 3
                    %                     stimulusClassDesc = 'Reapp_OD';
                    block_imageClass = 3;
                    block_task = 1;
                    block_imageXtask = 3;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 4
                    %                     stimulusClassDesc = 'Reapp_OT';
                    block_imageClass = 4;
                    block_task = 1;
                    block_imageXtask = 4;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 5
                    %                     stimulusClassDesc = 'Attend_SD';
                    block_imageClass = 1;
                    block_task = 2;
                    block_imageXtask = 5;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 6
                    %                     stimulusClassDesc = 'Attend_ST';
                    block_imageClass = 2;
                    block_task = 2;
                    block_imageXtask = 6;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 7
                    %                     stimulusClassDesc = 'Attend_OD';
                    block_imageClass = 3;
                    block_task = 2;
                    block_imageXtask = 7;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 8
                    %                     stimulusClassDesc = 'Attend_OT';
                    block_imageClass = 4;
                    block_task = 2;
                    block_imageXtask = 8;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 9
                    %                     stimulusClassDesc = 'Attend_Neutral';
                    block_imageClass = 5;
                    block_task = 2;
                    block_imageXtask = 9;
                else
                    error('not recognized')
                end
            elseif iStimSet == 2 %stimset B
                if scheduleTable{iStimSet}{iRun,1}{iRow,5} == 5
                    %                     stimulusClassDesc = 'Reapp_SD';
                    block_imageClass = 1;
                    block_task = 1;
                    block_imageXtask = 1;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 6
                    %                     stimulusClassDesc = 'Reapp_ST';
                    block_imageClass = 2;
                    block_task = 1;
                    block_imageXtask = 2;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 7
                    %                     stimulusClassDesc = 'Reapp_OD';
                    block_imageClass = 3;
                    block_task = 1;
                    block_imageXtask = 3;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 8
                    %                     stimulusClassDesc = 'Reapp_OT';
                    block_imageClass = 4;
                    block_task = 1;
                    block_imageXtask = 4;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 1
                    %                     stimulusClassDesc = 'Attend_SD';
                    block_imageClass = 1;
                    block_task = 2;
                    block_imageXtask = 5;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 2
                    %                     stimulusClassDesc = 'Attend_ST';
                    block_imageClass = 2;
                    block_task = 2;
                    block_imageXtask = 6;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 3
                    %                     stimulusClassDesc = 'Attend_OD';
                    block_imageClass = 3;
                    block_task = 2;
                    block_imageXtask = 7;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 4
                    %                     stimulusClassDesc = 'Attend_OT';
                    block_imageClass = 4;
                    block_task = 2;
                    block_imageXtask = 8;
                elseif scheduleTable{iStimSet}{iRun,1}{iRow,5} == 9
                    %                     stimulusClassDesc = 'Attend_Neutral';
                    block_imageClass = 5;
                    block_task = 2;
                    block_imageXtask = 9;
                else
                    error('not recognized')
                end
            else
                error('not recognized')
            end
            block_input = repmat([block_imageClass, block_task, block_imageXtask], [block_duration,1]);
            block_design(:,3:5) = block_input; % set input column
            DesignMatrix( iTime : iTime + block_duration -1, : ) = block_design;
            iTime = iTime + block_duration;
            clearvars block_*
            
            
            % 4) ISIA
            block_duration = scheduleTable{iStimSet}{iRun}{iRow,4};
            block_design = zeros( block_duration, DesignMatrix_classes );
            DesignMatrix( iTime : iTime + block_duration -1, : ) = block_design;
            iTime = iTime + block_duration;
            clearvars block_*
            
            
            % 5) RESPONSE
            block_duration = 4;
            block_design = zeros( block_duration, DesignMatrix_classes );
            block_input = 1;
            block_design(:,6) = block_input; % set input column
            DesignMatrix( iTime : iTime + block_duration -1, : ) = block_design;
            iTime = iTime + block_duration;
            clearvars block_*
            
            
        end
        
        % PARADIGM CLEANUP
        block_duration = 10;
        block_design = zeros( block_duration, DesignMatrix_classes );
        DesignMatrix( iTime : iTime + block_duration -1, : ) = block_design;
        iTime = iTime + block_duration;
        clearvars block_*
        
        
        %% Make SPM Files
        if iStimSet == 1
            thisSPM_set_label = 'a';
        elseif iStimSet == 2
            thisSPM_set_label = 'b';
        else
            error('')
        end
        thisSPM_suffix = sprintf('%s%d',thisSPM_set_label,iRun);
        
        ADHD_Dore4_SPMgenerate(DesignMatrix, thisSPM_suffix);
        
        %% Done
        clearvars SPM iTime image_idx iRow savenameSPM stimulusClassDesc
    end

end

end


function [ output_args ] = ADHD_Dore4_SPMgenerate( DesignMatrix, SPM_suffix )

leaveout = 8;

thisSPM_name = sprintf('SPM_reapp_attnd_%s',SPM_suffix);
SPM.labels = {'reappraise','attend_neg','attend_neu'};
SPM.design = zeros(size(DesignMatrix,1), length(SPM.labels));
thisSPM_key{1} = {[5],[1:4]};
thisSPM_key{2} = {[5],[5:8]};
thisSPM_key{3} = {[5],[9]};
% convert DesignMatrix into SPM design
for iColumn = 1:length(thisSPM_key)
    SPM.design(:,iColumn) = any(DesignMatrix(:,thisSPM_key{iColumn}{1}) == thisSPM_key{iColumn}{2},2);
end


generateSPM(SPM.design, leaveout, SPM.labels, [1:3], thisSPM_name)

clearvars SPM thisSPM*


thisSPM_name = sprintf('SPM_self_other_%s',SPM_suffix);
SPM.labels = {'R_SD','R_ST','R_OD','R_OT','A_SD','A_ST','A_OD','A_OT','A_Neu'};
SPM.design = zeros(size(DesignMatrix,1), length(SPM.labels));
thisSPM_key{1} = {[5],[1]};
thisSPM_key{2} = {[5],[2]};
thisSPM_key{3} = {[5],[3]};
thisSPM_key{4} = {[5],[4]};
thisSPM_key{5} = {[5],[5]};
thisSPM_key{6} = {[5],[6]};
thisSPM_key{7} = {[5],[7]};
thisSPM_key{8} = {[5],[8]};
thisSPM_key{9} = {[5],[9]};
% convert DesignMatrix into SPM design
for iColumn = 1:length(thisSPM_key)
    SPM.design(:,iColumn) = any(DesignMatrix(:,thisSPM_key{iColumn}{1}) == thisSPM_key{iColumn}{2},2);
end

generateSPM(SPM.design, leaveout, SPM.labels, [1:9], thisSPM_name)

clearvars SPM thisSPM*

end

function generateSPM(Design, leaveout, names, yonsets, saveName)

plottitle = regexprep(saveName,'_',' ');
for iCondition = 1:length(names)
    labels{iCondition} = regexprep(names{iCondition},'_',' ');
end

totalTime = size(Design,1);

if any(isnan(Design))
    error('NaN error')
end

onsets = repmat({[]},[length(names),1]);
durations = onsets;
durations_temp = repmat({[0]},[length(names),1]);
for iTime = leaveout+1:totalTime
    
    conditions = Design(iTime-1:iTime, :);
    contrast = conditions(2,:) - conditions(1,:);
    
    %     % find onsets
    %     a = find(contrast==1)
    %
    for iCondition = 1:size(contrast,2)
        switch contrast(iCondition)
            case 1
                onsets{iCondition} = [onsets{iCondition}, iTime-1-leaveout]; % onset is number of seconds preceeding the stim presentation
                durations_temp{iCondition} = 1;
            case -1
                durations{iCondition} = [durations{iCondition}, durations_temp{iCondition}];
            case 0
                durations_temp{iCondition} = durations_temp{iCondition} + 1;
            otherwise
                error('error')
        end
    end
    
end

Design_Collected = Design;
Design_Analyzed = Design;
Design_Analyzed = Design(leaveout+1:end,:);
Design(1:leaveout,:) = -1;

figure;
x=1:length(labels);
y=-1*(leaveout-1):totalTime-leaveout;
imagesc(x,y,Design);

ylabels = sort([0, onsets{yonsets}, totalTime-leaveout]);

set(gca,'Ytick',ylabels)
set(gca,'Xtick',1:length(labels),'XTickLabel',labels)
title({sprintf('Design: %s', plottitle),sprintf('DesignTime = %d, ScanTime = %d, LeftoutTime=%d', totalTime-leaveout, totalTime, leaveout)});
saveas(gcf,sprintf('%s.png', saveName));
close all;
save(sprintf('%s.mat', saveName),'names','onsets','durations','Design_Analyzed','Design_Collected');
end