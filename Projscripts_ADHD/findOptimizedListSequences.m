function BalancedLists = findOptimizedListSequences(funPrefix, FullList, nDesigns, nListsStored, BalancedLists, schedules, rng_currentstate, verbose)

if nargin < 8
    verbose = true;
end
% parameters for resumed runs
if nargin >= 7
    rng(rng_currentstate)
else
    rng_currentstate = 'default'
end
if nargin >= 5
    iStoredDesigns = size(BalancedLists,1) + 1
    iDesign = sum(~isnan(schedules(1,:)))+1;
end
% parameters for new runs
if nargin < 5
    iStoredDesigns = 1
    iDesign = 1;

    % initalization of randgen
    [~, rndidx] = feval(sprintf('%s_genRandIdx', funPrefix),FullList); %%%lazy workaround
    schedules = single(NaN(size(rndidx,1),nDesigns));
end

rng(rng_currentstate);

% optional progress counters for this execution
uniqueDesignsDetermined = 0;
nRandDesignsTried = 0;

fprintf('Beginning RandGen Loop\n');
while iDesign <= nDesigns
    clearvars rndidx temp_schedule temp_design tempBalancedList summaryStat store
    
    runImmediately = true; % generate first schedule before checking for uniqueness
    while runImmediately || any( all( bsxfun( @eq, schedules, single(rndidx) ), 1) )
        runImmediately = false;
        [temp_schedule, rndidx] = feval(sprintf('%s_genRandIdx', funPrefix),FullList); %%%lazy workaround
        nRandDesignsTried = nRandDesignsTried + 1;
        if verbose && ~mod(nRandDesignsTried,10000)
            fprintf('RandDesignsTried = %d\n',nRandDesignsTried);
        end
    end
    schedules(:,iDesign) = rndidx;
    if verbose && iDesign == 1
        fprintf('First new schedule generated\n')
    end
    
    % build design based on randomly generated indices
    temp_design = feval(sprintf('%s_buildDesign', funPrefix), FullList, temp_schedule);
    
    
    % validation
    [store, tempBalancedList] = feval(sprintf('%s_validateDesign', funPrefix), temp_design);
    
    
    % if design passes validation, store it
    if store
        BalancedLists(iStoredDesigns,:) = tempBalancedList;
        iStoredDesigns = iStoredDesigns + 1;
        uniqueDesignsDetermined = uniqueDesignsDetermined + 1;
    end
    
    
    if (~mod(iDesign,100000) && iDesign >= 200000) || (~mod(iDesign,10000) && iDesign < 200000 && iDesign >= 10000) || (~mod(iDesign,1000) && iDesign < 10000)
        fprintf('(%d unique designs, %d validated)  ',iDesign,uniqueDesignsDetermined);
        if iStoredDesigns > 1
            [~, summaryStat] = feval(sprintf('%s_sortAndTruncateStoredDesigns', funPrefix), BalancedLists, nListsStored);
            fprintf(': %s',summaryStat)
            clearvars summaryStat
        end
        fprintf('\n');
    end
    if ~mod(iDesign,100000) || iDesign == nDesigns
        % trim down stored designs
        if iStoredDesigns >= nListsStored
            [BalancedLists, ~] = feval(sprintf('%s_sortAndTruncateStoredDesigns', funPrefix), BalancedLists, nListsStored);
            iStoredDesigns = nListsStored + 1;
        end
        rng_currentstate = rng;
        save(sprintf('%s_intermediateWS.mat', funPrefix))
        fprintf('--intermediate workspace saved--\n');
        if iDesign == nDesigns
            save(sprintf('%s_FinalWS.mat', funPrefix));
            fprintf('--final workspace saved--\n');
        end
    end
    
    iDesign = iDesign + 1;
end
end

