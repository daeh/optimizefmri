function makeIAPSSubsetFolder( IAPSList, newDirName, saveDir )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

cd(saveDir);
mkdir(newDirName);
cd(newDirName);

[~, FullIAPS_allsubj_struct, ~] = getIAPSratings();

for iFile = 1:size(IAPSList,1)
    struct_index = find(strcmp({FullIAPS_allsubj_struct.IAPS}, num2str(IAPSList(iFile,1)))==1);
    copyfile(fullfile(FullIAPS_allsubj_struct(struct_index).folder, FullIAPS_allsubj_struct(struct_index).name),fullfile(saveDir,newDirName))
end


end

