function [BalancedLists, summaryStat] = ReappBallRuns_sortAndTruncateStoredDesigns(BalancedLists,nListsStored)

[~, sortidx] = sort([BalancedLists{:,6}], 'ascend');
BalancedLists = BalancedLists(sortidx,:);
if size(BalancedLists,1) < nListsStored
    nListsStored = size(BalancedLists,1);
end
BalancedLists = BalancedLists(1:nListsStored,:);

summaryStat = sprintf('mean (max (mean diffs)): %0.3d',BalancedLists{1,6});

end