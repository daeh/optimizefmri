function [newMapping, randidx] = ReappBallRuns_genRandIdx(MasterList)
randidx = [];
for iList = 1:length(MasterList)
    for iSublist = 1:length(MasterList{iList})
        newMapping{iList}{iSublist} = randperm(size(MasterList{iList}{iSublist},1));
        randidx = [randidx; newMapping{iList}{iSublist}'];
    end
end
end