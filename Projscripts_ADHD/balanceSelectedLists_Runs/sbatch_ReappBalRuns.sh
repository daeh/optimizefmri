#!/bin/bash
#SBATCH --job-name=ReappBalRunsOpt
#SBATCH --nodes=1 --cpus-per-task=12  --tasks-per-node=1
#SBATCH --mem=174186
#SBATCH --time=2-00:00
#SBATCH --mail-user=daeda@mit.edu --mail-type=ALL
#SBATCH --output=sbatchrunReappBalRunsOpt_stdout_%j.txt
#SBATCH --error=sbatchrunReappBalRunsOpt_stderr_%j.txt

module add openmpi/gcc/64/1.8.1
module add mit/matlab/2017a
cd /mindhive/gablab/u/daeda/analyses/ReappBalRunsOpt

srun matlab -nodisplay -nosplash -r "load('intermediateWS.mat'); findOptimizedListSequences('ReappBallRuns', FullList, 1500000, 50, BalancedLists, schedules, rng_currentstate, true); exit;"

# chmod +x sbatch_single.sh
# mpiexec -n 1 ./sbatch_single.sh

