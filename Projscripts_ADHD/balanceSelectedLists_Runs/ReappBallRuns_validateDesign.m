function [store, tempBalancedList] = ReappBallRuns_validateDesign(Design)

% Where each type of image is placed within the RunList
% Reappraise
idx{1} = 1:2; %SD
idx{2} = 3:4; %ST
idx{3} = 5:6; %OD
idx{4} = 7:8; %OT
% View
idx{5} = 9:10; %SD
idx{6} = 11:12; %ST
idx{7} = 13:14; %OD
idx{8} = 15:16; %OT
% Neutral
idx{9} = 17:24; %Neu

% calculates mean V & A for each idx (e.g. reapprase-SD)
%     dim 3 = V or A
%     dim 2 = runs
%     dim 1 = category average (mean of 2 images): 1 = view_SD, 2 = view_ST, 3 = view_OD, 4 = view_OT, 5:8 = reappraise_*, 9 = neutral
for iRun = 1:numel(Design)
    for iVA = 1:2
        for iIdx = 1:numel(idx)
            if iVA == 1
                VA = 2; % column corresponding to valence
            elseif iVA == 2
                VA = 4; % column corresponding to arousal
            end
            compared(iIdx,iRun,iVA) = mean(Design{iRun}(idx{iIdx},VA));
        end
    end
end

for iVA = 1:2
    %compare neutral
    compared2(1,iVA) = max(compared(9,:,iVA)) - min(compared(9,:,iVA));
    % neg reapp
    compared2(2,iVA) = max(mean(compared(1:4,:,iVA),1)) - min(mean(compared(1:4,:,iVA),1));
    % neg view
    compared2(3,iVA) = max(mean(compared(5:8,:,iVA),1)) - min(mean(compared(5:8,:,iVA),1));
    % neg self reapp
    compared2(4,iVA) = max(mean(compared(1:2,:,iVA),1)) - min(mean(compared(1:2,:,iVA),1));
    % neg self view
    compared2(5,iVA) = max(mean(compared(5:6,:,iVA),1)) - min(mean(compared(5:6,:,iVA),1));
    % neg other reapp
    compared2(6,iVA) = max(mean(compared(3:4,:,iVA),1)) - min(mean(compared(3:4,:,iVA),1));
    % neg other view
    compared2(7,iVA) = max(mean(compared(7:8,:,iVA),1)) - min(mean(compared(7:8,:,iVA),1));
end

tempBalancedList{1,1} = Design;
tempBalancedList{1,2} = compared;
tempBalancedList{1,3} = compared2;
tempBalancedList{1,4} = max(compared2(:,1));
tempBalancedList{1,5} = max(compared2(:,2));
tempBalancedList{1,6} = mean([tempBalancedList{1,4}; tempBalancedList{1,5}]);

store = true;

end