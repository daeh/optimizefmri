function [newDesign] = ReappBallRuns_buildDesign(MasterList, newMapping)

% Where each type of image is placed within the RunList
% Reappraise
idx{1} = 1:2; %SD
idx{2} = 3:4; %ST
idx{3} = 5:6; %OD
idx{4} = 7:8; %OT
% View
idx{5} = 9:10; %SD
idx{6} = 11:12; %ST
idx{7} = 13:14; %OD
idx{8} = 15:16; %OT
% Neutral
idx{9} = 17:24; %Neu

% Where each type of image is taken from the FullList
runCount{1} = [1:2;3:4;5:6;7:8];
runCount{2} = [1:2;3:4;5:6;7:8];
runCount{3} = [1:2;3:4;5:6;7:8];
runCount{4} = [1:2;3:4;5:6;7:8];
runCount{5} = [1:2;3:4;5:6;7:8];
runCount{6} = [1:2;3:4;5:6;7:8];
runCount{7} = [1:2;3:4;5:6;7:8];
runCount{8} = [1:2;3:4;5:6;7:8];
runCount{9} = [1:8;9:16;17:24;25:32];


nRuns = 4;


listID = [1 2 3 4 0]; % gives identifier to image category. SD = 1, ST = 2, OD = 3, OT = 4, Neutral = 0
sublistID = [1 2]; % gives identifier to instructions. Attend = 1, Reappraise = 2

% divy up available images within a catory to runs
for iList = 1:length(MasterList) % stim identity
    for iSublist = 1:length(MasterList{iList}) % 2 sets of stimuli
        for iRun = 1:nRuns
            if iSublist == 1 && iList ~= 5 % if reappraised images or ﾿﾿first half of neutral images??
                place = iList;
            elseif iSublist == 2 || iList == 5 % if viewed images or ﾿﾿second half of neutral images??
                place = 4 + iList;
            else
                error('error')
            end
            
            newDesign{iRun}(idx{place},:) = [MasterList{iList}{iSublist}( newMapping{iList}{iSublist}( runCount{place}(iRun,:) ) ,:), ...
                repmat(listID(iList),[length(idx{place}),1]), ...
                repmat(sublistID(iSublist),[length(idx{place}),1])];
        end
    end
end

end