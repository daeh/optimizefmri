function balanceSelectedLists_Runs()

%% Set Run Behavior
% runType = 'runImmediately'
runType = 'slurm'

nDesigns = 1500000;
nListsStored = 100;

%% Run
switch runType
case 'runImmediately'
load('/mindhive/gablab/users/daeda/balanceSelectedLists_Run/balanceSelectedLists_Set.mat');
clearvars -except BalancedLists

%% Get balancedLists from balanceSelectedLists_Set.m

SelectedList = 2; % which BalancedLists list to use

idxFull{1} = 1:8; %SD
idxFull{2} = 9:16; %ST
idxFull{3} = 17:24; %OD
idxFull{4} = 25:32; %OT

for iList = 1:4 % image identity
    for iSublist = 1:2 % image set
        FullList{iList}{iSublist} = BalancedLists{SelectedList,1}{iSublist}(idxFull{iList},:);
    end
end

clearvars -except FullList

%% Neutral
%%% should pull this from find neutral images
temp = load('/mindhive/gablab/users/daeda/balanceSelectedLists_Run/IAPS_neutral_subset.mat');
FullList{5}{1} = temp.IAPS_neutral_subset;

clearvars -except FullList

BalancedLists = findOptimizedListSequences('ReappBallRuns', FullList, 1500000, 50);

case 'slurm'

save('ReappBallRuns_FullList.mat','FullList');

%%% code for sbatch:
% load('ReappBallRuns_FullList.mat');
% BalancedLists = findOptimizedListSequences('ReappBallRuns', FullList, 1500000, 50, BalancedLists, schedules, rng_currentstate, true);

otherwise
	error('error')

end

%% 
% Where each type of image is placed within the RunList
% Reappraise
idx{1} = 1:2; %SD
idx{2} = 3:4; %ST
idx{3} = 5:6; %OD
idx{4} = 7:8; %OT
% View
idx{5} = 9:10; %SD
idx{6} = 11:12; %ST
idx{7} = 13:14; %OD
idx{8} = 15:16; %OT
% Neutral
idx{9} = 17:24; %Neu


%{
%%% This is done in ADHD_Dore4_CollectResults...

%% Sort by stimulus group
for iList = 1:size(BalancedLists2,1)
    for iRun = 1:numel(BalancedLists2{iList})
        for iIdx = 1:numel(idx)
            BalancedListsStimGrouped{iList,1}{iRun,1}{iIdx,1} = BalancedLists{iList}{iRun}(idx{iIdx},1);
        end
    end
end
BalancedListsStimGroupedIdxLabels{1,1} = 'StimSet_A SD';
BalancedListsStimGroupedIdxLabels{2,1} = 'StimSet_A ST';
BalancedListsStimGroupedIdxLabels{3,1} = 'StimSet_A OD';
BalancedListsStimGroupedIdxLabels{4,1} = 'StimSet_A OT';
BalancedListsStimGroupedIdxLabels{5,1} = 'StimSet_B SD';
BalancedListsStimGroupedIdxLabels{6,1} = 'StimSet_B ST';
BalancedListsStimGroupedIdxLabels{7,1} = 'StimSet_B OD';
BalancedListsStimGroupedIdxLabels{8,1} = 'StimSet_B OT';
BalancedListsStimGroupedIdxLabels{9,1} = 'Neutral';
%}

save('balanceSelectedLists_Run.mat')

fprintf('\n\nDone\n\n');

%% Preview Lists
for iList = 1:4
    for iRun = 1:4
        printListSummary( BalancedLists{iList}{iRun}, sprintf('ReappBalRuns_%03d_%03d.png',iList, iRun), '~/Desktop/ballruns', [0 0 20 20] )
    end
end


end