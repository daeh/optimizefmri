function [BalancedLists, summaryStat] = EmoLocNeu_sortAndTruncateStoredDesigns(BalancedLists,nListsStored)

[~, sortidx] = sort([BalancedLists{:,5}], 'ascend');
BalancedLists = BalancedLists(sortidx,:);
if size(BalancedLists,1) < nListsStored
    nListsStored = size(BalancedLists,1);
end
BalancedLists = BalancedLists(1:nListsStored,:);

summaryStat = sprintf('max (mean diff range)): %0.3d',BalancedLists{1,5});

end