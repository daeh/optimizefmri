function [newMapping, randidx] = EmoLocNeu_genRandIdx(MasterList)
newMapping = randperm(size(MasterList,1));
randidx = newMapping(1:60)';
end