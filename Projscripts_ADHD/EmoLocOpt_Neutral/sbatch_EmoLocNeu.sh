#!/bin/bash
#SBATCH --job-name=EmoBioListOpt
#SBATCH --nodes=1 --cpus-per-task=12  --tasks-per-node=1
#SBATCH --mem=74186
#SBATCH --time=3-00:00
#SBATCH --mail-user=daeda@mit.edu --mail-type=ALL
#SBATCH --output=sbatchrunEmoBioListOpt_stdout_%j.txt
#SBATCH --error=sbatchrunEmoBioListOpt_stderr_%j.txt

module add openmpi/gcc/64/1.8.1
module add mit/matlab/2017a
cd /mindhive/gablab/users/daeda/analyses/EmoLocOpt_Neutral

srun matlab -nodisplay -nosplash -r "load('FullListNeutral.mat'); findOptimizedListSequences('EmoLocNeu', FullList, 1500000, 50); exit;"
# srun matlab -nodisplay -nosplash -r "load('EmoLocNeu_intermediateWS.mat'); findOptimizedListSequences('EmoLocNeu', FullList, 1500000, 50, BalancedLists, schedules, rng_currentstate, true); exit;"

# chmod +x sbatch_single.sh
# mpiexec -n 1 ./sbatch_single.sh

